#ifndef SNIFF_FILTER_H
#define SNIFF_FILTER_H

/**
 * @brief attach a precompiled bpf "bpf_prof" to a given socket.
 * @param sock_fd socket to attach bpf to.
 * @return whether the bpf attached successfully or not
 */
bool attach_bpf(int sock_fd);

#endif // SNIFF_FILTER_H
