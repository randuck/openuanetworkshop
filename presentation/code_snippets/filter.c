#include <linux/filter.h>
#include <stdbool.h>
#include <sys/socket.h>

#include "filter.h"

// compiled bpf for the expression: "icmp[icmptype] != 0".
struct sock_filter filter_icmp_response[] = {
    {0x28, 0, 0, 0x0000000c}, {0x15, 0, 8, 0x00000800}, {0x30, 0, 0, 0x00000017},
    {0x15, 0, 6, 0x00000001}, {0x28, 0, 0, 0x00000014}, {0x45, 4, 0, 0x00001fff},
    {0xb1, 0, 0, 0x0000000e}, {0x50, 0, 0, 0x0000000e}, {0x15, 1, 0, 0x00000000},
    {0x6, 0, 0, 0x00040000},  {0x6, 0, 0, 0x00000000},
};

struct sock_fprog bpf_prog = {.filter = filter_icmp_response, .len = 11};

bool attach_bpf(int sock_fd) {
    return -1 != setsockopt(sock_fd, SOL_SOCKET, SO_ATTACH_FILTER, &bpf_prog, sizeof(bpf_prog));
}
