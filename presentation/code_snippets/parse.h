#ifndef SNIFF_PARSE_H
#define SNIFF_PARSE_H

#include <stdbool.h>
#include <stddef.h>

/**
 * parse ethernet frame according to eth_type and prints a string representation accordingly.
 * prints frame's raw data in hexdump format.
 * @param buff: raw ethernet frame buffer.
 * @param size total size of received ethernet frame.
 */
void parse_packet(const unsigned char *eth_frame, size_t len);

#endif // SNIFF_PARSE_H