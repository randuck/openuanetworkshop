#include "parse.h"
#include <arpa/inet.h>
#include <net/if.h>
#include <netinet/ether.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <netpacket/packet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define ARP_ETH_TYPE 0x0806
#define IP_ETH_TYPE 0x0800

#define ARP_REQUEST (0x01)
#define ARP_REPLY (0x02)

/* Ethernet ARP packet from RFC 826 */
struct arp_header {
    uint16_t hardware_type;
    uint16_t protocol_type;
    uint8_t hardware_len;
    uint8_t protocol_len;
    uint16_t opcode;
    uint8_t sender_mac[ETH_ALEN];
    uint32_t sender_ip;
    uint8_t target_mac[ETH_ALEN];
    uint32_t target_ip;
} __attribute__((packed));

/**
 * @return string representation of the current time.
 */
static char *current_time() {
    time_t now = time(0);
    struct tm *time_info = localtime(&now);
    return asctime(time_info);
}

/**
 * @brief   print given data in hexdump format.
 * @param data  buffer that contains raw ethernet frame's data
 * @param size  of the given data
 */
static void print_hexdump(const unsigned char *data, int size) {
    printf("-------------------- Hexdump --------------------\n");
    int counter = 0;
    for (int i = 0; i < size; ++i) {
        if (i % 16 == 0) {
            counter += 16;
            printf("\n%07x ", counter);
        } else if (i % 2 == 0) {
            printf(" ");
        }
        printf("%02x", data[i]);
    }
    printf("\n");
}

/**
 * @brief   extract source port & destination port from tcp segment
 * @param[in]   iph     ip header of received packet
 * @param[out]  sport   tcp source port
 * @param[out]  dport   tcp destination port
 */
static void parse_tcp_segment(struct iphdr *iph, int *sport, int *dport) {
    int iphdr_len = iph->ihl * 4;
    struct tcphdr *tcph = (struct tcphdr *)((char *)iph + iphdr_len);
    *sport = ntohs(tcph->source);
    *dport = ntohs(tcph->dest);
}

/**
 * @brief   extract source port & destination port from udp segment
 * @param[in]   iph     ip header of received packet
 * @param[out]  sport   udp source port
 * @param[out]  dport   udp destination port
 */
static void parse_udp_segment(struct iphdr *iph, int *sport, int *dport) {
    int iphdr_len = iph->ihl * 4;
    struct udphdr *udph = (struct udphdr *)((char *)iph + iphdr_len);
    *sport = ntohs(udph->source);
    *dport = ntohs(udph->dest);
}

/**
 * @brief   print a string representation of an ip packets in the following format:
 *      {timestamp} {protocol} ({source ip}:{source port} -> ({destination port:destination port})
 * @param buff  raw ethernet frame
 */
static void print_ip_packet(const unsigned char *buff) {
    int sport, dport;
    char transport_proto[4];

    // extract source & destination ip.
    struct iphdr *iph = (struct iphdr *)(buff + sizeof(struct ethhdr));
    struct in_addr source = {.s_addr = iph->saddr};
    struct in_addr destination = {.s_addr = iph->daddr};

    switch (iph->protocol) {
    case IPPROTO_TCP:
        parse_tcp_segment(iph, &sport, &dport);
        strcpy(transport_proto, "TCP");
        break;
    case IPPROTO_UDP:
        parse_udp_segment(iph, &sport, &dport);
        strcpy(transport_proto, "UDP");
        break;
    case IPPROTO_ICMP:
        printf("%sICMP (%s)->", current_time(), inet_ntoa(source));
        printf("(%s)\n", inet_ntoa(destination));
        return;
    default:
        printf("%sUNKNOWN[%d] (%s)->", current_time(), iph->protocol, inet_ntoa(source));
        printf("%s)\n", inet_ntoa(destination));
        return;
    }

    printf("%s%s (%s:%d) -> ", current_time(), transport_proto, inet_ntoa(source), sport);
    printf("(%s:%d)\n", inet_ntoa(destination), dport);
}

static void print_arp_packet(const unsigned char *buff) {
    // extract source & destination ip.
    struct arp_header *arph = (struct arp_header *)(buff + sizeof(struct ethhdr));
    struct in_addr target_a = {.s_addr = arph->target_ip};
    struct in_addr sender_a = {.s_addr = arph->sender_ip};

    switch (ntohs(arph->opcode)) {
    case ARP_REQUEST:
        printf("%sARP, Request who - has %s ", current_time(), inet_ntoa(target_a));
        printf("tell %s\n", inet_ntoa(sender_a));
        break;

    case ARP_REPLY:
        printf("%sARP, Reply %s is - at %02x:%02x:%02x:%02x:%02x:%02x\n", current_time(),
               inet_ntoa(sender_a), arph->sender_mac[0], arph->sender_mac[1], arph->sender_mac[2],
               arph->sender_mac[3], arph->sender_mac[4], arph->sender_mac[5]);
        break;

    default:
        // ...
        break;
    }
}

void parse_packet(const unsigned char *eth_frame, size_t size) {
    struct ethhdr *eth = (struct ethhdr *)eth_frame;
    int eth_proto = ntohs(eth->h_proto);
    switch (eth_proto) {
    case IP_ETH_TYPE:
        print_ip_packet(eth_frame);
        print_hexdump(eth_frame, size);
        break;
    case ARP_ETH_TYPE:
        print_arp_packet(eth_frame);
        print_hexdump(eth_frame, size);
    default:
        // printf("Unsupported protocol: %04x\n", eth_proto);
        break;
    }
}
