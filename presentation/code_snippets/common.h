#ifndef SNIFF_COMMON_H
#define SNIFF_COMMON_H

#include <stdbool.h>
#include <ctype.h>

// common macros.
#define CHECK(cond)                                                                                \
    ({                                                                                             \
        if (!(cond)) {                                                                             \
            goto error;                                                                            \
        }                                                                                          \
    })

#define CHECK_LOG(cond, error_fmt, ...)                                                            \
    ({                                                                                             \
        if (!(cond)) {                                                                             \
            fprintf(stderr, error_fmt "\n", ##__VA_ARGS__);                                        \
            goto error;                                                                            \
        }                                                                                          \
    })

#define CHECK_RET(cond, ret_val)                                                                   \
    ({                                                                                             \
        if (!(cond)) {                                                                             \
            return ret_val;                                                                        \
        }                                                                                          \
    })

#define CHECK_RET_FALSE(cond) CHECK_RET(cond, false)

/**
 * @brief   convert a given string into lowercase inplace
 */
static inline void to_lowercase(char *s)
{
    for (; *s; s++) *s=tolower(*s);
}

/**
 * @brief   whether given string is NULL or empty
 */
static inline bool is_empty_str(char *str) { return NULL == str || str[0] == '\0'; }

#endif // SNIFF_COMMON_H
