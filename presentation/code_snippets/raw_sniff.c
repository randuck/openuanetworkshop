/**
 * @file raw_sniff.c
 * @author Ido Kuks
 * @usage: ./raw_sniff [ -i interface ]
 * @build:
 *      (1) using Makefile:
 *           all        -> make
 *           standalone -> make raw
 *      (2) using gcc:
 *           gcc -Wall -Werror -o raw_sniff.c parse.c filter.c
 *
 * Minimal packet sniffer implemented using a raw socket.
 */

#include <arpa/inet.h>
#include <assert.h>
#include <errno.h>
#include <linux/if_ether.h>
#include <net/if.h>
#include <netpacket/packet.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <unistd.h>

#include "common.h"
#include "filter.h"
#include "parse.h"

#define ANY_IFACE "any"

unsigned char eth_frame[ETH_FRAME_LEN]; // ETH_FRAME_LEN = 1514

/**
 * @brief   bind a given raw socket to a given interface
 *
 * @param sock_fd: socket to bind
 * @param ifname: name of the interface name to bind to
 */
static bool bind_to_iface(int sock_fd, char *ifname) {
    struct ifreq ifr;
    struct sockaddr_ll sll;

    strncpy((char *)ifr.ifr_name, ifname, IF_NAMESIZE);
    // query the interface index by it's name using ioctl.
    CHECK_LOG(-1 != ioctl(sock_fd, SIOCGIFINDEX, &ifr), "Invalid interface: `%s`", ifname);
    sll.sll_family = AF_PACKET;
    sll.sll_ifindex = ifr.ifr_ifindex;
    sll.sll_protocol = htons(ETH_P_ALL);

    // bind the socket to a specific ifname using the crafted sockaddr.
    CHECK_LOG(-1 != bind(sock_fd, (struct sockaddr *)&sll, sizeof(sll)),
              "Failed binding interface: `%s`", ifname);

    return true;

error:
    return false;
}

/**
 * @brief   receive packets in a loop and print a string representation and hexdump for each.
 *
 * @param raw_sock  fd that is used for receiving packets.
 */
static void sniff_loop(int raw_sock) {
    while (true) {
        int len = recv(raw_sock, eth_frame, ETH_FRAME_LEN, 0);
        if (len < 0) {
            fprintf(stderr, "Failed receiving packet (errno: %d)\n", errno);
            break;
        }

        parse_packet(eth_frame, len);
    }
}

/**
 * @brief   create and configure a raw socket.
 *          bind to given iface (incase it's not null or 'any')
 *
 * @param[out] raw_socket pointer to populate with the created socket's fd.
 * @param[in]  ifname interface name to bind to (not binding incase of 'any', empty or NULL)
 */
static bool create_sniffer(int *raw_socket, char *ifname, bool filter_icmp) {
    // create raw socket.
    *raw_socket = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
    if (*raw_socket < 0) {
        CHECK_LOG(1 != errno, "Operation not permitted");
    }
    if (filter_icmp) {
        // attach a precompiled bpf to the socket.
        CHECK_LOG(attach_bpf(*raw_socket), "Failed attaching bpf filter");
    }
    // binds to a specific interface if given.
    if (!is_empty_str(ifname) && 0 != strcmp(ifname, ANY_IFACE)) {
        CHECK(bind_to_iface(*raw_socket, ifname));
        printf("Sniffing on: `%s`\n", ifname);
    } else {
        printf("Sniffing on: `%s`\n", ANY_IFACE);
    }

    return true;

error:
    close(*raw_socket);
    return false;
}

/**
 * @brief   print usage message
 * @param prog_name: full program name as it was used on execution.
 */
static void print_usage(char *prog_name) {
    fprintf(stderr, "Usage: %s [ -i interface ] [-f]\n", prog_name);
}

/**
 * @brief   parse program arguments from user input
 * @param[in]   argc
 * @param[in]   argv
 * @param[out]  ifname: interface name to fill from user's input
 * @param[out]  icmp_filter: flag which indicates whether to filter icmp or not
 * @return whether arguments are valid and parsed successfully
 */
static bool parse_args(int argc, char **argv, char *ifname, bool *icmp_filter) {
    assert(NULL != ifname);

    int op;
    int opt_argc = 1;
    bool given_iface = false;
    while (-1 != (op = getopt(argc, argv, "hfi:"))) {
        switch (op) {
        case 'h':
            print_usage(argv[0]);
            exit(0);

        case 'f':
            CHECK_LOG(!(*icmp_filter), "Multiple -f flags doesn't make any sense, please calm down "
                                       "and pass a single -f flag");
            opt_argc++;
            *icmp_filter = true;
            break;

        case 'i':
            CHECK_LOG(!given_iface, "Specifying multiple interfaces is not supported T_T");
            given_iface = true;
            opt_argc += 2;

            // validate and copy
            size_t iface_len = strlen(optarg);
            CHECK_LOG(iface_len > 0 && iface_len < IF_NAMESIZE, "Got iface name with invalid size");
            strncpy(ifname, optarg, IF_NAMESIZE);
            to_lowercase(ifname);
            break;

        default:
            print_usage(argv[0]);
            fprintf(stderr, "Got invalid options \"%c\"", (char)op);
            return false;
        }
    }

    CHECK_LOG(argc == opt_argc, "Got invalid args");
    return true;

error:
    print_usage(argv[0]);
    return false;
}

int main(int argc, char **argv) {
    char ifname[IF_NAMESIZE] = {0};
    bool icmp_filter = false;

    CHECK(parse_args(argc, argv, ifname, &icmp_filter));

    int raw_sock = -1;
    CHECK_LOG(create_sniffer(&raw_sock, ifname, icmp_filter), "Failed with errno: %d", errno);

    sniff_loop(raw_sock);
    close(raw_sock);
    return 0;

error:
    return 1;
}
