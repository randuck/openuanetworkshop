/**
 * @file raw_arp.c
 * @author Ido Kuks
 * @usage: ./raw_arp [ -i interface ]
 * @build:
 *      (1) using Makefile:
 *           all        -> make
 *           standalone -> make arp
 *      (2) using gcc:
 *           gcc -Wall -Werror -o raw_arp.c
 *
 * Showcase of inject a packet using raw socket with an example of an arp
 * request
 */

#include <arpa/inet.h>
#include <asm/types.h>
#include <assert.h>
#include <errno.h>
#include <linux/if_arp.h>
#include <linux/if_ether.h>
#include <linux/if_packet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <unistd.h>

#include "common.h"

#define BUF_SIZE (60)
#define IF_NAMESIZE (16)
#define IPV4_LENGTH (4)
#define MAC_LENGTH (6)

// arp
#define PROTO_ARP (0x0806)
#define ARP_REQUEST (0x01)
#define ARP_REPLY (0x02)
#define HW_TYPE (1)
#define ARP_PACKET_SIZE (sizeof(struct ethhdr) + sizeof(struct arp_header))

#define DIV "==================================================================================\n"

/* Ethernet ARP packet from RFC 826 */
struct arp_header {
    uint16_t hardware_type;
    uint16_t protocol_type;
    uint8_t hardware_len;
    uint8_t protocol_len;
    uint16_t opcode;
    uint8_t sender_mac[ETH_ALEN];
    uint32_t sender_ip;
    uint8_t target_mac[ETH_ALEN];
    uint32_t target_ip;
} __attribute__((packed));

/*
 * Sends an ARP who-has request to dst_ip
 * on interface ifindex, using source mac src_mac and source ip src_ip.
 */
bool send_arp(int sock_fd, int ifindex, const unsigned char *src_mac, uint32_t src_ip,
              uint32_t dst_ip) {

    uint8_t buffer[ARP_PACKET_SIZE] = {0};
    struct sockaddr_ll socket_address = {
        .sll_family = AF_PACKET,
        .sll_protocol = htons(ETH_P_ARP),
        .sll_ifindex = ifindex,
        .sll_hatype = htons(ARPHRD_ETHER),
        .sll_halen = ETH_ALEN,
        .sll_pkttype = PACKET_BROADCAST,
        .sll_addr[6] = 0x00,
        .sll_addr[7] = 0x00,
    };
    memcpy(socket_address.sll_addr, src_mac, ETH_ALEN);

    struct ethhdr *send_req = (struct ethhdr *)buffer;
    // set source to our mac address and broadcast destination
    memcpy(send_req->h_source, src_mac, sizeof(send_req->h_source));
    memset(send_req->h_dest, 0xff, sizeof(send_req->h_dest));
    send_req->h_proto = htons(PROTO_ARP);

    struct arp_header *arp_req = (struct arp_header *)(buffer + sizeof(struct ethhdr));

    /* Creating ARP request */
    // arp request
    arp_req->opcode = htons(ARP_REQUEST);
    // set arp request sender to our mac address and init target address
    // who-has settings
    arp_req->hardware_type = htons(HW_TYPE);
    arp_req->protocol_type = htons(ETH_P_IP);
    arp_req->hardware_len = ETH_ALEN;
    arp_req->protocol_len = IPV4_LENGTH;
    arp_req->sender_ip = src_ip;
    arp_req->target_ip = dst_ip;
    memset(arp_req->target_mac, 0x00, sizeof(arp_req->target_mac));
    memcpy(arp_req->sender_mac, src_mac, sizeof(arp_req->sender_mac));

    ssize_t len = sendto(sock_fd, buffer, ARP_PACKET_SIZE, 0, (struct sockaddr *)&socket_address,
                         sizeof(socket_address));

    struct in_addr sender_a = {.s_addr = arp_req->sender_ip};
    struct in_addr target_a = {.s_addr = arp_req->target_ip};
    printf("\nSent arp request\n\t");
    printf("([%02x:%02x:%02x:%02x:%02x:%02x] -> [ff:ff:ff:ff:ff:ff]) who - has %s ",
           send_req->h_source[0], send_req->h_source[1], send_req->h_source[2],
           send_req->h_source[3], send_req->h_source[4], send_req->h_source[5],
           inet_ntoa(target_a));
    printf("tell %s\n", inet_ntoa(sender_a));
    printf(DIV);

    CHECK_LOG(len >= 0, "Failed sending arp request (errno: %d)", errno);

    return true;

error:
    return false;
}

/**
 * @brief Get the iface index object
 *
 * @param sock_fd
 * @param ifname
 * @return int
 */
static int get_iface_index(int sock_fd, const char *ifname) {
    struct ifreq ifr;

    strncpy((char *)ifr.ifr_name, ifname, IF_NAMESIZE);
    // query the interface index by it's name using ioctl.
    if (0 != ioctl(sock_fd, SIOCGIFINDEX, &ifr)) {
        return -1;
    }

    return ifr.ifr_ifindex;
}

/**
 * @brief fills network information about an interface by it's name
 *
 * @param[in] sock_fd
 * @param[in] ifname    iface name
 * @param[out] ip       ip address of the given iface
 * @param[out] mac      mac address of the given iface
 * @param[out] ifindex  index of the given iface
 * @return whether all info was retrievd successfully
 */
static int get_iface_info(int sock_fd, const char *ifname, uint32_t *ip, uint8_t *mac,
                          int *ifindex) {
    // get iface index
    *ifindex = get_iface_index(sock_fd, ifname);
    CHECK_LOG(ifindex, "Failed retrieving index of iface: %s", ifname);

    struct ifreq ifr;
    strcpy(ifr.ifr_name, ifname);

    // get mac address
    CHECK_LOG(0 == ioctl(sock_fd, SIOCGIFHWADDR, &ifr), "Failed ioctl SIOCGIFHWADDR");
    memcpy(mac, ifr.ifr_hwaddr.sa_data, ETH_ALEN);

    // get ip address
    CHECK_LOG(0 == ioctl(sock_fd, SIOCGIFADDR, &ifr), "Failed ioctl SIOCGIFADDR");
    CHECK_LOG(ifr.ifr_addr.sa_family == AF_INET, "Expected an ipv4 iface");
    *ip = ((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr.s_addr;

    return true;

error:
    return false;
}

/**
 * @brief Blocks until
 *
 * @param sock_fd       socket to receive on
 * @param target_ip     ip address that we expect an arp response from
 * @return whether an arp response from the given ip was received successfully
 */
static bool recv_arp(int sock_fd, uint32_t target_ip) {
    unsigned char buffer[BUF_SIZE];
    ssize_t len;

    while ((len = recv(sock_fd, buffer, BUF_SIZE, 0)) >= 0) {
        struct ethhdr *eth_resp = (struct ethhdr *)buffer;
        int eth_type = ntohs(eth_resp->h_proto);
        // we are looking for an arp packet
        if (eth_type != PROTO_ARP) {
            continue;
        }

        struct arp_header *arp_resp = (struct arp_header *)(buffer + sizeof(struct ethhdr));
        int arp_opcode = ntohs(arp_resp->opcode);
        // we are looking for an arp response
        if (arp_opcode != ARP_REPLY) {
            continue;
        }

        // the response should be for the given target_ip
        if (arp_resp->sender_ip != target_ip) {
            continue;
        }

        struct in_addr sender_a = {.s_addr = arp_resp->sender_ip};
        struct in_addr target_a = {.s_addr = arp_resp->target_ip};
        printf("Got arp response!\n\t");
        printf("([%02x:%02x:%02x:%02x:%02x:%02x] -> [%02x:%02x:%02x:%02x:%02x:%02x]) %s is - at "
               "%02x:%02x:%02x:%02x:%02x:%02x\n",
               eth_resp->h_source[0], eth_resp->h_source[1], eth_resp->h_source[2],
               eth_resp->h_source[3], eth_resp->h_source[4], eth_resp->h_source[5],
               eth_resp->h_dest[0], eth_resp->h_dest[1], eth_resp->h_dest[2], eth_resp->h_dest[3],
               eth_resp->h_dest[4], eth_resp->h_dest[5], inet_ntoa(sender_a),
               arp_resp->sender_mac[0], arp_resp->sender_mac[1], arp_resp->sender_mac[2],
               arp_resp->sender_mac[3], arp_resp->sender_mac[4], arp_resp->sender_mac[5]);

        return true;
    }

error:
    return false;
}

/*
 * Sample code that sends an ARP who-has request on
 * interface <ifname> to IPv4 address <ip>.
 * Returns 0 on success.
 */
static bool arp_who_has_req(int sock_fd, const char *ifname, uint32_t target_ip) {
    uint32_t src_ip;
    uint8_t src_mac[ETH_ALEN] = {0};
    int ifindex;

    CHECK_RET_FALSE(get_iface_info(sock_fd, ifname, &src_ip, src_mac, &ifindex));

    CHECK_RET_FALSE(send_arp(sock_fd, ifindex, src_mac, src_ip, target_ip));
    CHECK_RET_FALSE(recv_arp(sock_fd, target_ip));

    return true;
}

/**
 * @brief   bind a given raw socket to a given interface
 *
 * @param sock_fd: socket to bind
 * @param ifname: name of the interface name to bind to
 */
static bool bind_to_iface(int sock_fd, char *ifname) {
    struct sockaddr_ll sll;

    sll.sll_family = AF_PACKET;
    sll.sll_ifindex = get_iface_index(sock_fd, ifname);
    sll.sll_protocol = htons(ETH_P_ALL);

    CHECK_LOG(sll.sll_ifindex >= 0, "Invalid iface: %s", ifname);
    // bind the socket to a specific ifname using the crafted sockaddr.
    CHECK_LOG(-1 != bind(sock_fd, (struct sockaddr *)&sll, sizeof(sll)),
              "Failed binding interface: `%s`", ifname);

    return true;

error:
    return false;
}

/**
 * @brief   create and configure a raw socket.
 *          bind to given iface (incase it's not null or 'any')
 *
 * @param[out] raw_socket pointer to populate with the created socket's fd.
 * @param[in]  ifname interface name to bind to (not binding incase of 'any',
 * empty or NULL)
 */
static bool create_arp_injector(int *raw_socket, char *ifname) {
    // create raw socket.
    *raw_socket = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ARP));
    if (*raw_socket < 0) {
        CHECK_LOG(1 != errno, "Operation not permitted");
    }

    CHECK(bind_to_iface(*raw_socket, ifname));
    printf("Using: `%s`\n", ifname);

    return true;

error:
    close(*raw_socket);
    return false;
}

/**
 * @brief   print usage message
 * @param prog_name: full program name as it was used on execution.
 */
static void print_usage(char *prog_name) {
    fprintf(stderr, "Usage: %s { interface_to_use } {target_ip}\n", prog_name);
}

/**
 * @brief   parse program arguments from user input
 * @param[in]   argc
 * @param[in]   argv
 * @param[out]  ifname: interface name to fill from user's input
 * @param[out]  src_ip: ip address to fill from user's input
 * @return whether arguments are valid and parsed successfully
 */
static bool parse_args(int argc, char **argv, char *ifname, uint32_t *src_ip) {
    assert(NULL != ifname);

    CHECK(argc == 3);

    // iface
    size_t iface_len = strlen(argv[1]);
    CHECK_LOG(iface_len > 0 && iface_len < IF_NAMESIZE, "Got iface name with invalid size");
    strncpy(ifname, argv[1], IF_NAMESIZE);
    to_lowercase(ifname);

    // ip address
    *src_ip = inet_addr(argv[2]);
    CHECK_LOG(*src_ip != 0 && *src_ip != 0xffffffff, "Got invalid ip address %s", argv[2]);

    return true;

error:
    print_usage(argv[0]);
    return false;
}

int main(int argc, char **argv) {
    char ifname[IF_NAMESIZE] = {0};
    uint32_t src_ip;

    CHECK(parse_args(argc, argv, ifname, &src_ip));

    int raw_socket;
    CHECK(create_arp_injector(&raw_socket, ifname));
    CHECK(arp_who_has_req(raw_socket, ifname, src_ip));

    return 0;

error:
    return 1;
}
