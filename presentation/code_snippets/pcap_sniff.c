/**
 * @file pcapdump.c
 * @author Ido Kuks
 * @usage: Usage: pcapdump [ -i interface ] [ bpf expression ]
 * @build:
 *      (1) using Makefile:
 *           all        -> make
 *           standalone -> make pcap
 *      (2) using gcc:
 *           gcc -lpcac -o pcapdump pcapdump.c parse.c
 *
 * Minimal packet sniffer implemented using a raw socket.
 */
#include <assert.h>
#include <net/if.h>
#include <pcap.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "common.h"
#include "parse.h"

#define MAX_FILTER_SIZE 256

/**
 * @brief print a list of the interfaces that exists on the machine
 */
static bool list_interfaces() {
    char errbuf[PCAP_ERRBUF_SIZE];
    pcap_if_t *alldevs;
    pcap_if_t *current;
    int i = 0;

    CHECK_LOG(0 == pcap_findalldevs(&alldevs, errbuf), "Couldn't print available devices.\n%s",
              errbuf);

    printf("Available interfaces:\n");
    for (current = alldevs; current != NULL; current = current->next) {
        printf("%d. %s\n", ++i, current->name);
    }
    printf("\n");

    pcap_freealldevs(alldevs);
    return true;

error:
    return false;
}

/**
 * @brief   choose default device to sniff on using `pcap_lookupdev`
 * @param[out] ifname   default iface name
 */
static bool set_default_iface(char *ifname) {
    char errbuf[PCAP_ERRBUF_SIZE];
    pcap_if_t *alldevsp = NULL;

    CHECK_LOG(0 == pcap_findalldevs(&alldevsp, errbuf), "Could not choose a default device\n%s",
              errbuf);
    CHECK_LOG(NULL != alldevsp, "No interfaces found");

    printf("default device: %s\n", alldevsp->name);
    // the first interface is picked
    strcpy(ifname, alldevsp->name);
    pcap_freealldevs(alldevsp);
    return true;

error:
    return false;
}

/**
 * @brief   print usage message
 * @param prog_name: full program name as it was used on execution.
 */
static void print_usage(char *prog_name) {
    fprintf(stderr, "Usage: %s [ -i interface ] { bpf expression }\n", prog_name);
}

/**
 * @brief   parse program arguments from user input
 * @param[in]   argc
 * @param[in]   argv
 * @param[out]  ifname interface name to fill from user's input
 * @param[out]  filter_exp  bpf filter expressions
 * @return whether arguments are valid and parsed successfully
 */
static bool parse_args(int argc, char **argv, char *ifname, char *filter_exp) {
    assert(NULL != ifname && NULL != filter_exp);

    int op;
    int opt_argc = 1;
    bool given_iface = false;
    while (-1 != (op = getopt(argc, argv, "hi:"))) {
        switch (op) {
        case 'h':
            print_usage(argv[0]);
            exit(0);

        case 'i':
            CHECK_LOG(!given_iface, "Specifying multiple interfaces is not supported T_T");
            given_iface = true;
            opt_argc += 2;

            // validate and copy
            size_t iface_len = strlen(optarg);
            CHECK_LOG(iface_len > 0 && iface_len < IF_NAMESIZE, "Got iface name with invalid size");
            strncpy(ifname, optarg, IF_NAMESIZE);
            to_lowercase(ifname);
            break;

        default:
            print_usage(argv[0]);
            fprintf(stderr, "Got invalid options \"%c\"", (char)op);
            return false;
        }
    }

    CHECK_LOG(argc <= opt_argc + 1, "Got invalid args");

    // set default device incase no iface was passed.
    if (!given_iface) {
        CHECK(set_default_iface(ifname));
    }

    // handle given bpf filter expression if supplied.
    int bpfarg_index = argc - 1;
    if (bpfarg_index == opt_argc) {
        size_t filter_len = strlen(argv[bpfarg_index]);
        CHECK_LOG(filter_len > 0 && filter_len < MAX_FILTER_SIZE,
                  "Got bpf expression with invalid size");
        strncpy(filter_exp, argv[bpfarg_index], MAX_FILTER_SIZE);
        to_lowercase(filter_exp);
    }

    return true;

error:
    print_usage(argv[0]);
    return false;
}

/**
 * @brief compile a given string filter-expression to a bpf program and apply it
 * @param handle            libpcap session handle
 * @param filter_expression pseudo filter string to is used in compilation to a bpf program
 * @param net               subnet that is used only when checking for IPv4 broadcast addresses in
 *                          the filter program.
 * @return whether bpf was compiled and attached successfully
 */
static bool set_bpf_filter(pcap_t *handle, char *filter_expression, bpf_u_int32 net) {
    struct bpf_program bpf_prog;

    CHECK_RET_FALSE(-1 != pcap_compile(handle, &bpf_prog, filter_expression, 0, net));
    CHECK_RET_FALSE(-1 != pcap_setfilter(handle, &bpf_prog));
    return true;
}

/**
 * @brief create and configure a libpcap sniffer using the given filter and ifname
 * @param[in]   ifname      interface name to listen on
 * @param[in]   filter_exp  bpf expression
 * @param[out]  handle      libpcap handle of the created sniffer
 **/
static bool create_sniffer(char *ifname, char *filter_exp, pcap_t **handle) {
    char errbuf[PCAP_ERRBUF_SIZE];
    bpf_u_int32 net;
    bpf_u_int32 mask = PCAP_NETMASK_UNKNOWN;

    // get network address and mask of the device.
    if (-1 == pcap_lookupnet(ifname, &net, &mask, errbuf)) {
        fprintf(stderr, "failed getting IPv4 details of the given interface with error - %s\n",
                errbuf);
        list_interfaces();
        return false;
    }

    // start sniffing using libpcap.
    *handle = pcap_open_live(ifname, BUFSIZ, 1, 1000, errbuf);
    CHECK_LOG(NULL != *handle, "Could not open device.\n%s\n", errbuf);

    // apply a filter if given.
    if (!is_empty_str(filter_exp)) {
        CHECK_LOG(set_bpf_filter(*handle, filter_exp, net),
                  "`%s` is an invalid filter expression.\n", filter_exp);
    }

    printf("Starting sniffing on: %s (filter: %s)\n", ifname,
           is_empty_str(filter_exp) ? "None" : filter_exp);
    return true;

error:
    if (NULL != *handle) {
        pcap_close(*handle);
    }
    return false;
}

/**
 * @brief   callback function that is called for every captured packet by libpcap.
 *          print a string representation for a given packet.
 * @param user      data that was passed to the callback using pcap_loop ( not used.. )
 * @param header    headers of the given packet.
 * @param packet    packet raw data.
 */
static inline void packet_handler(unsigned char *user, const struct pcap_pkthdr *header,
                           const unsigned char *packet) {
    parse_packet(packet, header->len);
}

int main(int argc, char **argv) {
    char ifname[IF_NAMESIZE] = {0};
    char filter_exp[MAX_FILTER_SIZE] = {0};

    CHECK(parse_args(argc, argv, ifname, filter_exp));

    pcap_t *handle = NULL;
    CHECK(create_sniffer(ifname, filter_exp, &handle));
    pcap_loop(handle, 0, packet_handler, NULL);
    pcap_close(handle);
    return 0;

error:
    return 1;
}
