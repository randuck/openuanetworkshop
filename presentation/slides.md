---
title: Raw Sockets & Libpcap
theme: blood
revealOptions:
    disableLayout: false
    slideNumber: c/t
    pdfSeparateFragments: false
---

# Raw Sockets & Libpcap

---

## Overview
### Background & Motivation

- Sniffing
- Man

----

## Overview
### Raw sockets
- What are raw sockets ?
- How to use them ?
- Why and when to use them ?
- How it works internally ? (Bonus)

----

## Overview
### Minimal packet parsing
- packet structure
- how to extract packet information

----

## Overview
### Packet Filtering
- What is BPF ?                 <!-- .element: class="fragment" data-fragment-index="1" -->
- Syntax and Examples           <!-- .element: class="fragment" data-fragment-index="2" -->
- Why and when to use them ?    <!-- .element: class="fragment" data-fragment-index="3" -->
- How it actually work ?        <!-- .element: class="fragment" data-fragment-index="4" -->

----

## Overview
### Injecting Packets

- How to inject packets using raw sockets ?
- Demo - arp request

----

## Overview
### Libpcap
- What is it for ?
- How to install ?
- How to use it ?
- Code examples

---

## Sniffing
### Why ?
1. To monitor and validate network traffic.
2. For Analyzing propietery protocols.
3. Traffic Analysis:

> The process of intercepting and examining messages in order to deduce information from patterns in communication

----

## Sniffing
#### How ?
|                  	| Wireshark                                                                                                                                                                                    	| Tcpdump                                                                        	|
|------------------	|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	|--------------------------------------------------------------------------------	|
| User Interface   	| GUI - Graphical user interface                                                                                                                                                               	| CLI-based                                                                      	|
| Examples         	| Follow stream that a selected packet belongs to.<br>Can decode data payloads if the encryption keys are identified.<br>Recognizes data payloads from file transfers such as smtp, http, etc. 	| Only provides do a simple analysis of such types of traffic (e.g. DNS queries) 	|
| Input Interfaces 	| Supports complex filters                                                                                                                                                                     	| Only simple filters                                                            	|
| Analysis         	| It does packet analysis and provides decoding of protocol-based packet capturing                                                                                                             	| Less efficient in decoding compared to Wireshark                               	|

----

## Sniffing
#### Extra
**`IMO `** `Tcpdump is more convinient for simple debugging cases  (e.g. Are my packets reaching an end point ?)`

**`Writing your own protocol parser `** `Wireshark also provide api for writing custom dissectors in lua.`

### Let's try sniffing !

----

## Man
Documentation in linux is done via man pages.
`man` is the system's manual pager.
```
man man
```

We will use `man` a lot for exploring stuff together throughout the presentation.

---

## Raw Sockets

1. `bsd` socket interface for send/receive operations.
<!-- .element: class="fragment" data-fragment-index="1" -->
2. There is no need to provide the port and IP address to a raw socket. <!-- .element: class="fragment" data-fragment-index="2" -->
3. Allows an application to directly access lower level protocols (before it's being processed by the kernel's TCP\IP stack).
   We can actually get the ethernet frames (Layer 2) and not just application. <!-- .element: class="fragment" data-fragment-index="3" -->

<img src="assets/raw_socket_payload_vs_other.jpg" alt="raw socket payload compared to other sockets" style="margin: auto; display: block;"> <!-- .element: class="fragment" data-fragment-index="3" -->
<br>

#### How to create <!-- .element: class="fragment" data-fragment-index="5" -->
```c
socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
```
<!-- .element: class="fragment" data-fragment-index="5" -->
----

### Let's break it down
* `man` for the resuce
    ```bash
    man -f packet
    # incase we didn't know that a man page for packet existed
    man -k packet | less
    # packet (7)           - packet interface on device level
    ```
* Cool there a is a manpage under `Miscellaneous`

----

### Let's break it down

Now let's get some answers from the man page
```bash
man 7 packet
```

#### Requirements
<!-- .element: class="fragment" data-fragment-index="1" -->
> In order to create a packet socket, a process must have the CAP_NET_RAW capability in the user namespace that governs its network namespace.
<!-- .element: class="fragment" data-fragment-index="1" -->
----
### Let's break it down

Now let's get some answers from the man page
```bash
man 7 packet
```
#### 1. Why `domain` = `AF_PACKET` ?
<!-- .element: class="fragment" data-fragment-index="1" -->
> Packet sockets are used to receive or send raw packets at the device driver (OSI Layer 2) level. They allow the user to implement protocol modules in user space on top of the physical layer.
<!-- .element: class="fragment" data-fragment-index="1" -->
----

### Let's break it down

Now let's get some answers from the man page
```bash
man 7 packet
```

#### 2. Why `type` = `SOCK_RAW` ?
<!-- .element: class="fragment" data-fragment-index="1" -->
> either SOCK_RAW for raw packets including the link-level header or SOCK_DGRAM for cooked packets with the link-level header removed.
> SOCK_RAW for raw packets including the link-level header
<!-- .element: class="fragment" data-fragment-index="1" -->
----

### Let's break it down

Now let's get some answers from the man page
```bash
man 7 packet
```

#### 3. Why `protocol` = `htons(ETH_P_ALL)` ?
<!-- .element: class="fragment" data-fragment-index="1" -->
> protocol is the IEEE 802.3 protocol number in network byte order. <br>
> See the <linux/if_ether.h> for a list of allowed protocols.
> When protocol is set to htons(ETH_P_ALL), then all protocols are received.
<!-- .element: class="fragment" data-fragment-index="1" -->
Other protocols for example: `AF_INET` for ipv4, `AF_INET6` for ipv6, ...

----

### Binding the raw socket to a specific interface
#### 1. Bind
```
man 2 bind
# int bind(int sockfd, const struct sockaddr *addr, socklen_t addrlen);
```

But struct sockaddr doesn't seems to be relevant... &#129300;
```c
struct sockaddr {
    sa_family_t sa_family;
    char        sa_data[14];
}
```

----

### Binding the raw socket to a specific interface
#### 1. Bind

According to the man page:
> The purpose of this 'struct sockaddr' is to cast the socket address structure in order to avoid compiler warnings. <br>

- We should use a relevant sockaddr struct for raw_socket -> `sockaddr_ll` (ll for link-layer).
- `struct sockaddr_ll` has a member called `ssl_ifindex` for specifying interface index to bind to.

* So we can bind the raw socket this way:
```c
sll.sll_family = AF_PACKET;
sll.sll_ifindex = <iface_index>;
sll.sll_protocol = htons(ETH_P_ALL);
// bind the socket to a specific ifname using the crafted sockaddr.
bind(sock_fd, (struct sockaddr *)&sll, sizeof(sll));`
```

----

### Binding the raw socket to a specific interface
#### 2. Getting interface index

```bash
man netdevice
```
> Ioctls
> ...
>       SIOCGIFINDEX:
>              Retrieve the interface index of the interface into ifr_ifindex.

* So:
```c
# ifname is string that contains the interface name
strncpy((char *)ifr.ifr_name, ifname, IF_NAMESIZE);
// query the interface index by it's name using ioctl.
CHECK_LOG(-1 != ioctl(sock_fd, SIOCGIFINDEX, &ifr), "Invalid interface: `%s`", ifname);
sll.sll_family = AF_PACKET;
sll.sll_ifindex = ifr.ifr_ifindex;
sll.sll_protocol = htons(ETH_P_ALL);
```

----

### Binding the raw socket to a specific interface
*Finally we get the following code*
```c
/**
 * @brief   bind a given raw socket to a given interface
 *
 * @param sock_fd: socket to bind
 * @param ifname: name of the interface name to bind to
 */
static bool bind_to_iface(int sock_fd, char *ifname) {
    struct ifreq ifr;
    struct sockaddr_ll sll;

    strncpy((char *)ifr.ifr_name, ifname, IF_NAMESIZE);
    // query the interface index by it's name using ioctl.
    CHECK_LOG(-1 != ioctl(sock_fd, SIOCGIFINDEX, &ifr), "Invalid interface: `%s`", ifname);
    sll.sll_family = AF_PACKET;
    sll.sll_ifindex = ifr.ifr_ifindex;
    sll.sll_protocol = htons(ETH_P_ALL);

    // bind the socket to a specific ifname using the crafted sockaddr.
    CHECK_LOG(-1 != bind(sock_fd, (struct sockaddr *)&sll, sizeof(sll)),
              "Failed binding interface: `%s`", ifname);

    return true;

error:
    return false;
}
```

----
## Sniffing !
```c
static void sniff_loop (int raw_sock) {
    while (true) {
        int len = recv(raw_sock, eth_frame, ETH_FRAME_LEN, 0);
        if (len < 0) {
            // error handle
        }
        parse_packet(eth_frame, len);
    }
}
```

---

## Minimal packet parsing
#### L2 - Eth frame

* _Format_
<img src="assets/Ethernet_Type_II_Frame_format.svg.png" alt="Ethernet II frame format" style="margin: auto; display: block;">
<br>

```c
struct ethhdr *eth = (struct ethhdr *)eth_frame;
int eth_proto = ntohs(eth->h_proto);
```
----

## Minimal packet parsing
#### L2 - Eth frame

* We need to parse the rest of the packet according to its `eth type`
```c
struct ethhdr *eth = (struct ethhdr *)eth_frame;
int eth_proto = ntohs(eth->h_proto);
switch (eth_proto) {
case IP_ETH_TYPE: // 0x0800
    // parse ipv4 packet
    break;
case ARP_ETH_TYPE: // 0x0806
    // parse arp packet
default:
    printf("Unsupported protocol: %04x\n", eth_proto);
    break;
}
```

----

## Minimal packet parsing
#### L3 - IPv4
* Let's say we got an ipv4 packet  `eth type`:
<img src="assets/IPv4_Packet_format.svg.png" alt="ipv4 packet format" style="margin: auto; display: block;">
<br>

----

## Minimal packet parsing
#### L3 - IPv4
1. ipv4 header comes right after
```c
struct iphdr *iph = (struct iphdr *)(buff + sizeof(struct ethhdr));
```
2. extract ip addresses
```
struct in_addr source_a = {.s_addr = iph->saddr};
struct in_addr destination_a = {.s_addr = iph->daddr};
printf("(%s) -> (%s)", inet_ntoa(source_a), inet_ntoa(destination_a));
```

----

## Minimal packet parsing
#### L3 - IPv4
3. parse next layer according to packet's transport layer protocol
```c
switch (iph->protocol) {
    case IPPROTO_TCP:
        // parse tcp segment
        break;
    case IPPROTO_UDP:
        // parse udp segment
        break;
    case IPPROTO_ICMP:
        // parse icmp layer
        break;
    default:
        // not supported protocol
        return;
    }
```

----

#### L4 - TCP\UDP

* extract port numbers

```c [1 | 3-6 | 8-12]
int iphdr_len = iph->ihl * 4;

// for tcp
struct tcphdr *tcph = (struct tcphdr *)((char *)iph + iphdr_len);
int sport = ntohs(tcph->source);
int dport = ntohs(tcph->dest);

// for udp
struct udphdr *udph = (struct udphdr *)((char *)iph + iphdr_len);
int sport = ntohs(udph->source);
int dport = ntohs(udph->dest);
```

----

#### Packet hexdump
```c
/**
 * @brief   print given data in hexdump format.
 * @param data  buffer that contains raw ethernet frame's data
 * @param size  of the given data
 */
static void print_hexdump(const unsigned char *data, int size) {
    printf("-------------------- Hexdump --------------------\n");
    int counter = 0;
    for (int i = 0; i < size; ++i) {
        if (i % 16 == 0) {
            counter += 16;
            printf("\n%07x ", counter);
        } else if (i % 2 == 0) {
            printf(" ");
        }
        printf("%02x", data[i]);
    }
    printf("\n");
}
```

---

## Filtering
`Filtering the sniffed packets can be very useful !`

**There are two different kinds of filters:**
  1. `Display Filter` - While intercepting all incoming packets, view just a subset of the packets by filtering the packet in usermode context.
    In other words, display filter is used for hiding some of the packets from the packet list
       * Most of the times we would want to sniff specific type packets (e.g. specific host or protocol).

  2. `Capture Filter` - intercept only wanted subset of the incoming packets
       * Used for reducing the size of a raw packet capture.

----

### BPF = Berkley packet filter
`A filter program that specifies which packets it wants to receive`
#### Syntax examples
  1. **DNS**
        ```
        udp dst port not 53
        ```
        <!-- .element: class="fragment" data-fragment-index="1" -->
  2. **Specific Mac Address**
        ```
        ether host 11:22:33:44:55:66
        ```
        <!-- .element: class="fragment" data-fragment-index="2" -->
  3. **TCP-SYN packet**
        ```
        tcp[13] & 2 != 0
        ```
        <!-- .element: class="fragment" data-fragment-index="3" -->

----

## How BPF actually work ?
The string description of a filter is compiled into a dedicated 32bit instruction set with fixed-length instructions and two registers.
Let's use tcpdump for seeing the instruction for a given filter and walkthrough it.

```bash [1 | 2-3 | 4-5 | 6-7 | 8-9 | 10-11 | 12-13 | 1-13]
tcpdump -d icmp
# load offset 12 which contains the `eth type`
(000) ldh      [12]
# if eth type == 0x800 (IPv4) continue, otherwise drop
(001) jeq      #0x800           jt 2    jf 5
# load offset 23 which contains the `ipv4 type`
(002) ldb      [23]
# if ipproto == 0x1 (icmp) continue, otherwise drop
(003) jeq      #0x1             jt 4    jf 5
# accept
(004) ret      #262144
# drop
(005) ret      #0
```

----

### Extra

* Some links for further reading:
    * [IBM BPF Examples](https://www.ibm.com/docs/en/qsip/7.4?topic=queries-berkeley-packet-filters)
    * [Linux Socket Filtering](https://www.infradead.org/~mchehab/kernel_docs/networking/filter.html)
    * [Know Thy BPF](https://gist.github.com/errzey/1111503/bbcda355e8ffbf5141dc10e0e551eb6edf666e36)

**Notes**:
1. The Linux kernel provides an extended version of the BPF filtering mechanism, called eBPF, which uses a JIT mechanism, and which is used for packet filtering, as well as for other purposes in the kernel`
2. Nowadays BFS is also called `cbpf` for `classic bpf`.

----

### Adding a capture filter to our raw socket

* First We will use tcpdump's feature for dumping bpf instructions of a filter.
  This time we will want to see the raw bytes (in c array format)
```bash
# This time we will filter only icmp *responses*
sudo tcpdump -dd "icmp[icmptype] != 0"
Warning: assuming Ethernet
{ 0x28, 0, 0, 0x0000000c },
{ 0x15, 0, 3, 0x00000800 },
{ 0x30, 0, 0, 0x00000017 },
{ 0x15, 0, 1, 0x00000001 },
{ 0x6, 0, 0, 0x00040000 },
{ 0x6, 0, 0, 0x00000000 },
```

----

For attaching the above filter we simply need to do the following:
  1. Create `struct sock_fprog` which wraps the raw bytes of the bpf filter's instructions.
  2. set `SO_ATTACH_FILTER` socket option and pass the bpf prog

```c
#include <linux/filter.h>
#include <stdbool.h>
#include <sys/socket.h>

#include "filter.h"

// compiled bpf for the expression: "icmp[icmptype] != 0".
struct sock_filter filter_icmp_response[] = {
    {0x28, 0, 0, 0x0000000c}, {0x15, 0, 8, 0x00000800},
    {0x30, 0, 0, 0x00000017}, {0x15, 0, 6, 0x00000001},
    {0x28, 0, 0, 0x00000014}, {0x45, 4, 0, 0x00001fff},
    {0xb1, 0, 0, 0x0000000e}, {0x50, 0, 0, 0x0000000e},
    {0x15, 1, 0, 0x00000000}, {0x6, 0, 0, 0x00040000},
    {0x6, 0, 0, 0x00000000},
};

struct sock_fprog bpf_prog = {.filter = filter_icmp_response, .len = 11};

bool attach_bpf(int sock_fd) {
    return -1 != setsockopt(sock_fd, SOL_SOCKET, SO_ATTACH_FILTER, &bpf_prog, sizeof(bpf_prog));
}
```

---

## Demo
### raw_sniff
```bash
git https://gitlab.com/randuck/openuanetworkshop.git
cd openuanetworkshop/presentation/code_snippets/
make raw

# sniff on any
sudo ./raw_sniff
# specify interface
sudo ./raw_sniff -i eth0
# icmp only
sudo ./raw_sniff -i eth0 -f
```

---

### Injecting Packets

1. Create raw socket for injecting packets

```c
socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ARP));
```

2. Bind to interface to inject through
```c
// As we did for before for sniffing
```

3. Get information of the selected iface (ip address, mac address)
```c
// Using ioctl's like we did for retrieving the iface's index
```

4. Create `sockaddr_ll` with iface informations

5. Build an ethernet frame

6. Inject
```c
ssize_t len = sendto(sock_fd, eth_frame, size, 0, (struct sockaddr *)&socket_address,
                         sizeof(socket_address));
```

----

## Demo
### raw_arp
```bash
git https://gitlab.com/randuck/openuanetworkshop.git
cd openuanetworkshop/presentation/code_snippets/
make arp pcap

# Usage: sudo ./raw_arp {interface} {target_ip}

# sniff arp packets
sudo ./pcap_sniff "arp"
# inject 'who-has' ip address `172.31.80.1` arp request through `eth0`
sudo ./raw_arp eth0 172.31.80.1
```

---

## Libpcap
* Portable C/C++ library for network traffic capture.
* [Open source](https://github.com/the-tcpdump-group/libpcap).
* Very cross platform, compatible with many architecture and os's (even shit like hp-ux, solaris, ...)
* Provide the packet-capture and filtering engines of many open-source and commercial network tools, including protocol analyzers (packet sniffers), network monitors, network intrusion detection systems, traffic-generators and network-testers.
* `tcpdump` is developed by same group (the-tcpdump-group), as such it is based on it.

----

## Installation
* Using your package manager
    ```bash
    # e.g. on ubuntu based distros
    sudo apt-get install libpcap-dev
    ```
* Building from sources ([INSTALL.md](https://github.com/the-tcpdump-group/libpcap/blob/master/INSTALL.md))
    ```bash
    git clone https://github.com/the-tcpdump-group/libpcap.git
    cd libpcap
    ./configure
    make
    sudo make install
    ```

----

## Usage

1. Link with the library by specifying:
```bash
gcc * -lpcap
```
2. Include libpcap header
```c
#include <pcap.h>
```
3. Sniff

```c
Much Code Such Wow
```

----

## Cheatsheet

1. `man -k pcap`

2. <!-- .element: class="fragment" data-fragment-index="1" --> pcap_findalldevs, pcap_freealldevs - get a list of capture devices, and free that list

```c
// list all interfaces
pcap_if_t *alldevsp = NULL;
// return values should be checked
if (0 != pcap_findalldevs(&alldevs, errbuf) {
    // error handle
}
for (current = alldevs; current != NULL; current = current->next) {
    printf("%d. %s\n", ++i, current->name);
}
printf("\n");
```
<!-- .element: class="fragment" data-fragment-index="1" -->

----

## Cheatsheet
3. pcap_lookupnet - find the IPv4 network number and netmask for a device
```c
char errbuf[PCAP_ERRBUF_SIZE];
bpf_u_int32 net;
bpf_u_int32 mask = PCAP_NETMASK_UNKNOWN;
pcap_lookupnet(ifname, &net, &mask, errbuf)
```

4. <!-- .element: class="fragment" data-fragment-index="1" --> pcap_open_live - open a device for capturing
```c
pcap_t *handle = pcap_open_live(ifname, BUFSIZ, 1, 1000, errbuf);
```
<!-- .element: class="fragment" data-fragment-index="1" -->

5. <!-- .element: class="fragment" data-fragment-index="2" --> pcap_compile, pcap_setfilter - used to compile a string into a filter program and to apply a filter program to the stream of packets
```c
struct bpf_program bpf_prog;
CHECK_RET_FALSE(-1 != pcap_compile(handle, &bpf_prog filter_expression, 0, net));
CHECK_RET_FALSE(-1 != pcap_setfilter(handle, &bpf_prog));
```
<!-- .element: class="fragment" data-fragment-index="2" -->

----

## Cheatsheet

6. pcap_loop - process packets from a live capture or savefile

```c
static inline void packet_handler(unsigned char *user, const struct pcap_pkthdr *header,
                           const unsigned char *packet) {
    // will be called for each packet
    parse_packet(packet, header->len);
}

pcap_loop(handle, 0, packet_handler, NULL);
```

7. pcap_close - close a capture device or savefile
<!-- .element: class="fragment" data-fragment-index="1" -->

---

## Demo
### pcap_sniff
```bash
git https://gitlab.com/randuck/openuanetworkshop.git
cd openuanetworkshop/presentation/code_snippets/
make pcap

# Usage: ./pcap_sniff [ -i interface ] { bpf expression }
sudo ./pcap_dump
# specify interface
sudo ./pcap_sniff -i eth0
# icmp only
sudo ./pcap_sniff -i eth0 "icmp"
```

---

## Thanks For Listening

* [Presentation and Resources](https://gitlab.com/randuck/openuanetworkshop/-/tree/main/presentation)


<img src="assets/thanks_for_listening.png" class="bg">



<style>
h1, h2, h3 {
    text-align: center;
}

span {
    font-size: 0.9em
}

.reveal .slides {
    text-align: left;
}

li, tbody {
    text-align: left;
    font-size: 0.6em;
}

.reveal p {
    text-align: left;
    font-size: 0.55em;
}

.reveal li p {
    font-size: 0.8em;
}

.reveal blockquote {
    width: 100%;
}

blockquote {
  background: #f9f9f9;
  border-left: 10px solid #ccc;
  margin: 1.5em 10px;
  padding: 0.5em 10px;
  quotes: "\201C""\201D""\2018""\2019";
  margin: 0;
  float: left;
}
blockquote:before {
  color: #ccc;
  font-size: 4em;
  line-height: 0.1em;
  margin-right: 0.25em;
  vertical-align: -0.4em;
}

.reveal blockquote p {
  margin: 0.1;
  float: inline;
  font-size: 0.5em;
}

.reveal code {
  resize: both;
}

.reveal .bg {
    margin: auto;
    display: block;
}

.reveal h4 {
    color: green;
}

</style>
