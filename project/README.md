# OS Fingerprint

Project for `networks workshop` in openua (20588)

https://www.openu.ac.il/courses/20588.htm

## Description
`osscan` is a python package which aims to mimic nmap os fingerprinting capabilities `nmap -O`

## Installation
`osscan` comes as a python package with an entrypoint (console_script)
```bash
git clone https://gitlab.com/randuck/openuanetworkshop.git
cd openuanetworkshop/project
python setup.py install
```

## Usage
```bash
$ osscan --help

Usage: osscan [OPTIONS] TARGET_HOST

  Create a fingerprint of the given target sending probes to target and
  performing tests that analyze the target's responses

Options:
  -v, --verbose                   Print debug logs
  -i, --iface TEXT                Output interface to bind to for network
                                  probes
  -u, --closed-udp-port INTEGER RANGE
                                  Close udp port in the given target which
                                  will be used for probes  [1<=x<=65535;
                                  required]
  -c, --closed-tcp-port INTEGER RANGE
                                  Close tcp port in the given target which
                                  will be used for probes  [1<=x<=65535;
                                  required]
  -t, --open-tcp-port INTEGER RANGE
                                  Open tcp port in the given target which will
                                  be used for probes  [1<=x<=65535; required]
  --help                          Show this message and exit.
```

## Demo
![OSScan Demo](docs/osscan_demo.gif)
## How It Works
1. Probe the target with a total of 16 packets with different options and in the different protocols
   (TCP, UDP, ICMP).  
   *See:* **nmap documentation:** [nmap osdetect probes sent](https://nmap.org/book/osdetect-methods.html#osdetect-probes), **osscan code:** [probes](https://gitlab.com/randuck/openuanetworkshop/-/blob/main/project/osscan/probes.py)

2. Analyze the target's responses on the probes by performing different tests in order to create
   a FingerPrint of the target (which matches nmap's fingerprint format).  
   *See:* **nmap documentation:** [nmap osdetect response tests](https://nmap.org/book/osdetect-methods.html#osdetect-response-tests), **osscan code:** [fingerprinter](https://gitlab.com/randuck/openuanetworkshop/-/blob/main/project/osscan/fingerprinter/fingerprinter.py)
    ``` bash
    # An example fingerprint in nmap's format:
    SEQ(SP=C9%GCD=1%ISR=CF%TI=Z%CI=Z%II=I%TS=A)
    OPS(O1=M400CST11NW5%O2=M400CST11NW5%O3=M400CNNT11NW5%
        O4=M400CST11NW5%O5=M400CST11NW5%O6=M400CST11)
    WIN(W1=8000%W2=8000%W3=8000%W4=8000%W5=8000%W6=8000)
    ECN(R=Y%DF=Y%T=40%W=8018%O=M400CNNSNW5%CC=N%Q=)
    T1(R=Y%DF=Y%T=40%S=O%A=S+%F=AS%RD=0%Q=)
    T2(R=N)
    T3(R=Y%DF=Y%T=40%W=8000%S=O%A=S+%F=AS%O=M400CST11NW5%RD=0%Q=)
    T4(R=Y%DF=Y%T=40%W=0%S=A%A=Z%F=R%O=%RD=0%Q=)
    T5(R=Y%DF=Y%T=40%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)
    T6(R=Y%DF=Y%T=40%W=0%S=A%A=Z%F=R%O=%RD=0%Q=)
    T7(R=Y%DF=Y%T=40%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)
    U1(R=Y%DF=N%T=40%IPL=164%UN=0%RIPL=G%RID=G%RIPCK=G%RUCK=G%RUD=G)
    IE(R=Y%DFI=N%T=40%CD=S)
    ```
    *See:* [osdetect fingerprint format](https://nmap.org/book/osdetect-fingerprint-format.html)

3. Parse nmap os fingerprints database and match the generated target fingerprint against every fingerprint in the db.  
*See:* **nmap documentation:** [nmap os matching algorithm](https://nmap.org/book/osdetect-guess.html), **osscan code:** [parser](https://gitlab.com/randuck/openuanetworkshop/-/blob/main/project/osscan/nmap_os_db_parser/fingerprint.py), [matcher](https://gitlab.com/randuck/openuanetworkshop/-/blob/main/project/osscan/matching.py)

1. For every match, a confidence factor is calculated, osscan record the fingerprints with the highest confidence factor and finally log a summary of the detection

