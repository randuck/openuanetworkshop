#!/usr/bin/env python3
"""
Remote OS detection using TCP/IP stack fingerprinting
"""
__author__ = "Ido Kuks"
__version__ = "0.1"

import logging
import sys
from ipaddress import ip_address
from socket import IPPROTO_TCP, IPPROTO_UDP

import click

from .common.types import TargetData
from .fingerprinter.fingerprinter import FingerPrinter
from .logging import logging_init
from .matching import FingerPrintMatcher
from .nmap_os_db_parser.fingerprint_db import FingerPrintDB
from .scanner import is_host_up, validate_port_state


@click.argument("target_host", callback=lambda _ctx, _param, addr: str(ip_address(addr)))
@click.option("--open-tcp-port",
              "-t",
              required=True,
              type=click.IntRange(1, 65535),
              help="Open tcp port in the given target which will be used for probes")
@click.option("--closed-tcp-port",
              "-c",
              required=True,
              type=click.IntRange(1, 65535),
              help="Close tcp port in the given target which will be used for probes")
@click.option("--closed-udp-port",
              "-u",
              required=True,
              type=click.IntRange(1, 65535),
              help="Close udp port in the given target which will be used for probes")
@click.option("--iface", "-i", default=None, help="Output interface to bind to for network probes")
@click.option("--verbose", "-v", is_flag=True, default=False, help="Print debug logs")
@click.command(no_args_is_help=True)
def main(target_host: str, open_tcp_port: int, closed_tcp_port: int, closed_udp_port: int,
         iface: str, verbose: bool) -> None:
    """
    Create a fingerprint of the given target sending probes to target and performing tests
    that analyze the target's responses
    """
    logging_init(verbose)
    # used for calling custom log levels (currently only success)
    logger = logging.getLogger()

    logging.debug(f"Making sure {target_host} is up")
    if not (avg_rtt := is_host_up(target_host)):
        logging.error(f"Failed, host seems to be down {target_host}")
        sys.exit(1)

    logger.success(f"Target {target_host} is up")

    validate_port_state(target_host, open_tcp_port, IPPROTO_TCP, True, avg_rtt)
    validate_port_state(target_host, closed_tcp_port, IPPROTO_TCP, False, avg_rtt)
    validate_port_state(target_host, closed_udp_port, IPPROTO_UDP, False, avg_rtt)

    target_data = TargetData(target_host, open_tcp_port, closed_tcp_port, closed_udp_port, iface)
    target_fp = FingerPrinter(target_data).fingerprint()
    logging.info(f"TCP/IP fingerprint:\n{target_fp}")

    db = FingerPrintDB()
    matcher = FingerPrintMatcher(db)
    matcher.match(target_fp)

    logging.info(matcher.summary())
    confidence = matcher.best_match_confidence
    if confidence > 80:
        logger.success(
            f'Seems the target is running:\n\t- {matcher.best_match_description} [confidence: {confidence}%]')
    else:
        logger.guess(
            f'Not sure but guessing that the target is running:\n\t- {matcher.best_match_description} [confidence: {confidence}%]')


if __name__ == "__main__":
    main()
