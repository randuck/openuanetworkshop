from dataclasses import dataclass
from typing import Optional


@dataclass
class TargetData:
    """
    target information for probing
    """
    ip_address: str
    open_tcp_port: int
    closed_tcp_port: int
    closed_udp_port: int
    iface: Optional[str] = None
