"""
Common osscan exceptions
"""


class InvalidPortState(Exception):
    pass


class MatchTypeError(TypeError):
    def __init__(self, cls, got, *args):
        super().__init__(args)
        self.cls = cls
        self.got = got

    def __repr__(self):
        return f"Matching {self.cls.__class__.__name__} with {type(self.got)} is not supported"
