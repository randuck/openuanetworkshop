from typing import Any, List, Mapping, Tuple, Union

from .exceptions import MatchTypeError

T_TESTS = ["R", "DF", "T", "TG", "W", "S", "A", "F", "O", "RD", "Q"]
SUPPORTED_TESTS = {
    "SEQ": ["R", "SP", "GCD", "ISR", "TI", "CI", "II", "SS", "TS"],
    "OPS": ["R"] + [f'O{i}' for i in range(1, 7)],
    "WIN": ["R"] + [f'W{i}' for i in range(1, 7)],
    "ECN": ["R", "DF", "T", "TG", "W", "O", "CC", "Q"],
    **{f"T{i}": T_TESTS for i in range(1, 8)},
    "U1": ["R", "DF", "T", "TG", "IPL", "UN", "RIPL", "RID", "RIPCK", "RUCK", "RUD"],
    "IE": ["R", "DFI", "T", "TG", "CD"],
}


class Test:
    """
    Represent a single test ({test_name}={test_result})
    """
    OMIT = None

    def __init__(self, name: str, result_expr: Any):
        self.test_name = name
        self.test_result = result_expr

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Test):
            raise MatchTypeError(self, other)

        if other.test_name != self.test_name:
            raise TypeError("Trying to match different tests ({other.test_name} and {self.test_name}) "
                            "doesn't make any sense 0-o")
        return other.test_result == self.test_result

    def __repr__(self) -> str:
        if self.test_result == self.OMIT:
            return ""

        result = self.test_result
        if isinstance(self.test_result, bool):
            result = "Y" if result else "N"
        elif isinstance(self.test_result, int):
            result = f"{self.test_result:X}"
        return f"{self.test_name}={result}"


class TestsType:
    """
    Represents a generic test type (category) i.e SEQ, OPS, ...
    """
    TEST_SEP = "%"

    def __init__(self, *tests: Test):
        assert self.__class__.__name__.endswith("Tests")
        self.name: str = self.__class__.__name__[:-5].upper()
        self.tests: Mapping[str, Test] = {
            test.test_name: test for test in tests if test.test_result != Test.OMIT}
        self._validate_test_type()

    def _validate_test_type(self):
        """
        Make sure given tests type is supported and contains only fitting tests
        """
        if self.name not in SUPPORTED_TESTS:
            raise ValueError(f"Defined an unsupported TestType: {self.name}")

        unsupported_tests = []
        for test in self.tests:
            if test not in SUPPORTED_TESTS[self.name]:
                unsupported_tests.append(test)

        if unsupported_tests:
            raise ValueError(
                f"Found unsupported tests under {self.name}: {unsupported_tests}")

    @classmethod
    def from_dict(cls: object, type_name: str, tests_dict: dict) -> object:
        """
        Create a TestsType instance using a dict that maps test results by the respected test names
        """
        assert type_name in TEST_TYPES
        from osscan.nmap_os_db_parser.fingerprint import parse_test_result

        tests = [Test(name, parse_test_result(result)) for name, result in tests_dict.items() if result != Test.OMIT]
        return TEST_TYPES[type_name](*tests)

    def match(self, other: object, tests_type_points: Mapping[str, int]) -> Tuple[int, int]:
        """
        Match this TestsType instance against a given one which is used as a reference.
        Calculate the match confidence factor which is defined to be: possible_point / match_points
        while `possible_points` are the total points of existing tests in the reference tests type
        and   `match_points`    are the total points of tests with a matching result
        Return the confidence factor
        """
        if not isinstance(other, TestsType):
            raise MatchTypeError(self, other)
        if other.name != self.name:
            raise ValueError("Trying to match different test types ({other.name} and {this.name}) "
                             "doesn't make any sense 0-o")

        # sum of points of **existing** matching tests between fingerprint and the reference fingerprint
        possible_points = 0
        # sum of points of the actual matching test results
        match_points = 0

        for test_name, test in self.tests.items():
            if test_name not in other.tests:
                # skip tests which doesn't exists in the reference tests type
                continue

            test_points = tests_type_points[test_name]
            possible_points += test_points
            if other.tests[test_name] == test:
                # add matched test results points
                match_points += test_points
        return (match_points, possible_points)

    def __repr__(self) -> str:
        return f"{self.name}({self.TEST_SEP.join(map(repr, self.tests.values()))})"


# Create TypeTests classes dynamically (Some metaprogramming magic 8D)
TEST_TYPES = {test_type: type(f'{test_type}Tests', (TestsType,), {})
              for test_type in SUPPORTED_TESTS}


class TestsPoints(dict):
    """
    Represent and handle interaction with a mapping between each test in each test line and its points
    """
    PREFIX = "MatchPoints"

    def __init__(self, points_dict):
        # given points_dict must contains tests points for each supported tests type
        if points_dict.keys() != SUPPORTED_TESTS.keys():
            raise ValueError(
                f"Can't create a TestsPoints, missing point for test types: {SUPPORTED_TESTS.keys() - points_dict.keys()}")
        self.tests_points: Mapping[str, Mapping[str, int]] = points_dict

    @classmethod
    def with_equal_points(cls, points=1):
        """
        Create a TestsPoints instance with equal points for every test
        """
        return cls({test_type: {test: points for test in tests} for test_type, tests in SUPPORTED_TESTS.items()})

    def __getitem__(self, identifier) -> Union[Mapping[str, int], int]:
        """
        Incase TestsPoints is indexed by a single string that index is treated as the tests type so we return a mapping
        between each test in the corresponding tests type with its points.
        incase TestsPoints is indexed by two strings each they treated as [tests_type, test_name] and the points of that
        test under the given tests_type is returned
        if identifier is not valid because of format or it just doesn't exists an appropriate exception is raised.
        """
        if isinstance(identifier, str):
            tests_type = identifier
            return self.tests_points[tests_type]
        elif isinstance(identifier, tuple):
            if len(identifier) != 2:
                raise IndexError(
                    f"Expected two identifiers [tests_type, test_name] but got {len(identifier)} identifiers")
            tests_type, test_name = identifier
            return self.tests_points[tests_type][test_name]
        raise IndexError(
            f"Cannot use {type(identifier)} as in index of TestsPoints")

    def __repr__(self) -> str:
        representation = ""
        for test_type, test_type_points in self.tests_points.items():
            representation += f'{test_type}:\n'
            for test_name, test_points in test_type_points.items():
                representation += f"\t- {test_name}={test_points}\n"
        return representation


class FingerPrint:
    """
    Represents a fingerprint
    """

    def __init__(self, seq: TestsType, ops: TestsType, win: TestsType, ecn: TestsType,
                 t_1: TestsType, t_2: TestsType, t_3: TestsType, t_4: TestsType,
                 t_5: TestsType, t_6: TestsType, t_7: TestsType, u_1: TestsType,
                 ie: TestsType, description: str = ""):
        self.description = description
        self.test_lines: List[TestsType] = [
            seq, ops, win, ecn, t_1, t_2, t_3, t_4, t_5, t_6, t_7, u_1, ie]
        self._validate_test_lines()

    def _validate_test_lines(self):
        """
        Validate given test line are supported and from the expected TestsType
        """
        for test_line in self.test_lines:
            if test_line.name not in TEST_TYPES:
                raise TypeError(
                    f"Got Unknown test line: {test_line.name}")
            if not isinstance(test_line, TEST_TYPES[test_line.name]):
                raise TypeError(
                    f"Expected {test_line.name} test line to be an instance of: {TEST_TYPES[test_line.name]}")

    @classmethod
    def from_dict(cls, tests_types_dict: dict, description: str = ""):
        """
        Create a FingerPrint instance using a dict that maps test line dicts by the respected test line names
        """
        if tests_types_dict.keys() != SUPPORTED_TESTS.keys():
            raise ValueError(
                f"Can't create a fingerprint, missing test types: {SUPPORTED_TESTS.keys() - tests_types_dict.keys()}")

        tests_types = [TestsType.from_dict(tests_type, tests) for (tests_type, tests) in tests_types_dict.items()]
        return cls(*tests_types, description)

    def __eq__(self, other: object):
        if not isinstance(other, FingerPrint):
            raise MatchTypeError(self, other)
        test_points = TestsPoints.with_equal_points()
        return self.match(other, test_points) == 1

    def match(self, other: object, test_points: TestsPoints) -> float:
        """
        Match this FingerPrint instance against a given one which is used as a reference.
        Calculate the final match confidence factor which is defined to be: possible_point / match_points
        while `possible_points` are the total points of existing tests in the reference fingerprint
        and   `match_points`    are the total points of tests with a matching result
        Return the confidence factor
        """
        if not isinstance(other, FingerPrint):
            raise MatchTypeError(self, other)

        total_possible_points = 0
        total_match_points = 0
        # accumulate possible and match point for each matching test_type
        for tests_type, other_tests_type in zip(self.test_lines, other.test_lines):
            tests_type_points = test_points[tests_type.name]
            test_line_match_points, test_line_possible_points = tests_type.match(other_tests_type, tests_type_points)
            total_match_points += test_line_match_points
            total_possible_points += test_line_possible_points

        confidence_factor = total_match_points / total_possible_points
        return confidence_factor

    def __repr__(self) -> str:
        return f" |   {'-'*5} {self.description} {'-'*5}\n | " \
            + "\n | ".join(map(repr, self.test_lines))
