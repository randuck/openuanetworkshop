import logging
from enum import IntEnum
from random import randint
from socket import IPPROTO_TCP, IPPROTO_UDP
from typing import Optional

from scapy.layers.inet import ICMP, IP, TCP, UDP
from scapy.sendrecv import send, sr, sr1

from .common.exceptions import InvalidPortState
from .common.types import TargetData

MAX_PORT = 2**16-1  # 16 bits field


class PortState(IntEnum):
    OPEN = 1
    CLOSED = 2
    FILTERED = 4
    UNKNOWN = 8


def is_host_up(host_ip: str, n: int = 4, timeout: int = 1) -> Optional[int]:
    """
    pings given target n times and calculate the average rtt
    incase target didn't respond to any of the ping until timeout is passed, NULL is returned
    """
    avg_rtt = None
    echo_request = IP(dst=host_ip) / ICMP()
    answered, _unanswered = sr([echo_request] * n, timeout=timeout, verbose=False)
    if answered:
        avg_rtt = sum([ping.answer.time - ping.query.sent_time for ping in answered]) / len(answered)
    return avg_rtt


def validate_port_state(target_host: str, port: int, protocol: int, should_be_open: bool, avg_rtt: int) -> None:
    """
    make sure supplied given port's state is valid
    """
    assert protocol in [IPPROTO_TCP, IPPROTO_UDP]

    timeout = avg_rtt + 0.1
    protocol_str = 'TCP' if protocol == IPPROTO_TCP else 'UDP'
    expected_str = 'open' if should_be_open else 'closed'

    logging.debug(f"Making sure that {protocol_str} port {port} at {target_host} is {expected_str} as expected")
    if protocol == IPPROTO_TCP:
        port_state = scan_tcp_port(target_host, port, timeout)
    else:
        port_state = scan_udp_port(target_host, port, timeout)

    if port_state == PortState.UNKNOWN:
        logging.error(f"Could not determine the state of {protocol_str} port {port}")
        raise InvalidPortState()

    if port_state & PortState.FILTERED:
        logging.warn(f"Given {protocol_str} port {port} was expected to be "
                     f"{expected_str} at {target_host} but seems to be filtered by "
                     "the host (possibly a firewall), continuing..")
        return

    # FILTERED is no longer an option
    is_open = (port_state == PortState.OPEN)
    if (should_be_open ^ is_open):
        logging.critical(f"Given {protocol_str} port {port} was expected to be {expected_str} at {target_host} "
                         "but seems to be false")
        raise InvalidPortState()
    logging.getLogger().success(f"Given {protocol_str} port {port} at {target_host} is {expected_str} as expected")


def scan_tcp_port(target_host: str, port: int, timeout: int) -> PortState:
    """
    Tries to identify a given tcp port's state in a given target similarly to nmap's "Stealth Syn Scan".
    We send a TCP SYN packet to target's port and analyze according to the response:
    * TCP SYN-ACK       -> port is open, we send a TCP RST packet to signal the target to close the session.
    * TCP RST           -> port is closed.
    * No Response       -> port is probably filtered (by a firewall or something).
    * Icmp Unreachable  -> port is probably filtered (by a firewall or something).
    * Other             -> port state is unknown.
    see: https://nmap.org/book/synscan.html#scan-methods-tbl-syn-scan-responses
    """
    reply = sr1(IP(dst=target_host) / TCP(dport=port, flags="S"), timeout=timeout, verbose=False)
    if not reply:
        return PortState.FILTERED
    if reply.haslayer(TCP):
        if reply[TCP].flags == "SA":
            send(IP(dst=target_host) / TCP(sport=reply[TCP].dport, dport=port, flags="R"), verbose=False)
            return PortState.OPEN
        return PortState.CLOSED
    if reply.haslayer(ICMP):
        # icmp unreachable
        if reply[ICMP].type == 3:
            return PortState.FILTERED
    return PortState.UNKNOWN


def scan_udp_port(target_host: str, port: int, timeout: int) -> PortState:
    """
    Tries to identify a given udp port's state in a given target similarly to nmap's "Stealth Syn Scan".
    We send a TCP SYN packet to target's port and analyze according to the response:
    * TCP SYN-ACK       -> port is open, we send a TCP RST packet to signal the target to close the session.
    * TCP RST           -> port is closed.
    * No Response       -> port is probably filtered (by a firewall or something).
    * Icmp Unreachable  -> port is probably filtered (by a firewall or something).
    * Other             -> port state is unknown.
    see: https://nmap.org/book/synscan.html#scan-methods-tbl-syn-scan-responses
    """
    reply = sr1(IP(dst=target_host) / UDP(dport=port), timeout=timeout, verbose=False)
    if not reply:
        return PortState.FILTERED | PortState.OPEN
    if reply.haslayer(UDP):  # unusual
        return PortState.OPEN
    if reply.haslayer(ICMP):
        # icmp unreachable
        if reply[ICMP].type == 3:
            if reply[ICMP].code == 3:
                return PortState.CLOSED
            return PortState.FILTERED
    return PortState.UNKNOWN


def get_random_high_port():
    return 30000 + randint(0, 14781)


def scan_for_missing_ports(target_data: TargetData, start: int = 1, end: int = MAX_PORT, timeout: int = 0.2):
    for port in range(start, end+1):
        if not target_data.open_tcp_port:
            if PortState.OPEN == scan_tcp_port(target_data.ip_address, port, timeout):
                target_data.open_tcp_port = port
        if not target_data.closed_tcp_port:
            if PortState.CLOSED == scan_tcp_port(target_data.ip_address, port, timeout):
                target_data.closed_tcp_port = port
        if not target_data.closed_udp_port:
            if PortState.CLOSED == scan_udp_port(target_data.ip_address, port, timeout):
                target_data.closed_udp_port = port

    if not target_data.open_tcp_port:
        logging.critical("Couldn't find an open tcp port, terminating operation T_T")
        raise InvalidPortState()
    if target_data.closed_tcp_port is None:
        logging.warn("Couldn't find a closed tcp port (Possibly because ports a filtered by a firewall on the host)"
                     "Generating a random high port and hope for the best")
        target_data.closed_tcp_port = get_random_high_port()
    if target_data.closed_udp_port is None:
        logging.warn("Couldn't find a closed udp port (Possibly because ports a filtered by a firewall on the host)"
                     "Generating a random high port and hope for the best")
        target_data.closed_udp_port = get_random_high_port()
