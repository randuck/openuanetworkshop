from typing import Any, Dict, List, Tuple

from osscan.common.exceptions import MatchTypeError
from osscan.common.fingerprint import (TEST_TYPES, FingerPrint, Test,
                                       TestsPoints, TestsType)

TESTS_SEP = "%"
TEST_NAME_RESULT_SEP = "="
OR_OPERATOR_SYMBOL = "|"
RANGE_OPERATOR_SYMBOL = "-"
LESS_THAN_OPERATOR_PREFIX = "<"
GREATER_THAN_OPERATOR_PREFIX = ">"
BOOL_SYMBOLS = ['Y', 'N']


class Range:
    """
    Represent the (inclusive) RANGE operator of the nmap test expression.
    Matches against a numeric value which falls within the range specified ({start}-{end})
    """

    def __init__(self, start: int, end: int):
        assert isinstance(start, int) and isinstance(end, int)
        self.start = start
        self.end = end

    def __eq__(self, other: object) -> bool:
        if isinstance(other, Range):
            return self.start == other.start and self.end == other.end
        if other == Test.OMIT:
            return False
        if isinstance(other, str):
            try:
                other = int(other, 16)
            except ValueError:
                return False
        if not isinstance(other, int):
            raise MatchTypeError(self, other)
        return other >= self.start and other <= self.end

    def __repr__(self) -> str:
        return f'{self.start:X}-{self.end:X}'


class GreaterThan:
    """
    Represent the GREATER_THAN operator of the nmap test expression.
    Matches against a numeric value which is greater than the operator value (>{value})
    """

    def __init__(self, minimum: int):
        self.minimum = minimum

    def __eq__(self, other: object) -> bool:
        if isinstance(other, GreaterThan):
            return self.minimum == other.minimum
        if other == Test.OMIT:
            return False
        if isinstance(other, str):
            try:
                other = int(other, 16)
            except ValueError:
                return False
        if not isinstance(other, int):
            raise MatchTypeError(self, other)
        return other > self.minimum

    def __repr__(self) -> str:
        return f'>{self.minimum}'


class LessThan:
    """
    Represent the LESS_THAN operator of the nmap test expression.
    Matches against a numeric value which is less than the operator value (<{value})
    """

    def __init__(self, maximum: int):
        self.maximum = maximum

    def __eq__(self, other: object) -> bool:
        if isinstance(other, LessThan):
            return self.maximum == other.maximum
        if other == Test.OMIT:
            return False
        if isinstance(other, str):
            try:
                other = int(other, 16)
            except ValueError:
                return False
        if not isinstance(other, int):
            raise MatchTypeError(self, other)
        return other < self.maximum

    def __repr__(self) -> str:
        return f'<{self.maximum:X}'


class Or:
    """
    Represent the OR operator of the nmap test expression.
    Matches against any of the clauses ([|]{value}(|{value})*)
    Note that an initial pipe symbol means than an empty option will match too
    """

    def __init__(self, options: List[Any]):
        self.options = options

    def __eq__(self, other: object) -> bool:
        if isinstance(other, Or):
            unmatched_options = self.options.copy()
            for option in other.options:
                try:
                    unmatched_options.remove(option)
                except ValueError:
                    return False
            return not unmatched_options
        for option in self.options:
            try:
                if other == option:
                    return True
            except MatchTypeError:
                pass
        return False

    def __repr__(self) -> str:
        if not self.options:
            return "|"
        return "|".join(map(str, self.options))


def _parse_hexnum(hexdigits: str):
    return int(hexdigits, 16)


def parse_test_result(test_result: str) -> Any:
    """
    Parses the given data according to: https://nmap.org/book/osdetect-fingerprint-format.html#osdetect-test-expressions
    A test result can be one of the following:
        Word:                   One character or more that represent a value that is relevant for the test
                                (e.g. "SA" which represent Syn-Ack TCP Flags for the F test)
        Hex Number:             (e.g B00000)
        Range Operator:         <start>"-"<end> while start, end are hex numbers (e.g 3B-45)
        Greater-Than Operator:  ">"<minimum>    while minimum is a hex number (e.g. >B00000)
        Less-Than Operator:     "<"<maximum>    while maximum is a hex number (e.g. <B00000)
        Or Operator:            [|]<result_1>|<result_2> while every result is one of the above not including the Or operator
                                if the result starts with the or operator an empty value is a result

    Args:
        test_result (str): textual test result which following the format of one of the above options

    Returns:
        Any: an instance of an appropriate object which represent the given data
    """
    if isinstance(test_result, int):
        return f'{test_result:X}'
    if OR_OPERATOR_SYMBOL in test_result:
        options = [parse_test_result(option) for option in test_result.split(OR_OPERATOR_SYMBOL)]
        return Or(options)
    elif RANGE_OPERATOR_SYMBOL in test_result:
        start, end = test_result.split(RANGE_OPERATOR_SYMBOL)
        return Range(_parse_hexnum(start), _parse_hexnum(end))
    elif test_result.startswith(LESS_THAN_OPERATOR_PREFIX):
        maximum = _parse_hexnum(test_result[1:])
        return LessThan(maximum)
    elif test_result.startswith(GREATER_THAN_OPERATOR_PREFIX):
        minimum = _parse_hexnum(test_result[1:])
        return GreaterThan(minimum)
    elif test_result in BOOL_SYMBOLS:
        return test_result == "Y"
    return test_result


def _parse_test(test: str) -> Test:
    """
    Parse a single Test that is expected to following the following format:
        <test_name>=<test_result>
    The test_result is parsed using the helper function above

    Args:
        test (str): test textual representation

    Returns:
        Test: a Test instance that represent the given data
    """
    test_name, test_result = test.split(TEST_NAME_RESULT_SEP)
    return Test(test_name, parse_test_result(test_result))


def _get_test_line_info(tests_line: str) -> Tuple[str, List[str]]:
    """
    Extracts tests type and a list of textual tests from a given tests_line
    e.g. "DOGE(SUCH=TEST%MUCH=WOW)" => "DOGE", ["SUCH=TEST", "MUCH=WOW"]
    """
    tests_start, tests_end = tests_line.index("("), tests_line.index(")")
    tests_type = tests_line[:tests_start]
    tests_str = tests_line[tests_start+1:tests_end]
    return tests_type, tests_str.split(TESTS_SEP)


def _parse_tests_line(tests_line: str) -> TestsType:
    """
    Parses a test line that is expected to follow the following format:
     <type_name>(test1=test_result%test2=test_result2%...)\n
    A "=" symbol separates between a test's name and its value
    A "%" symbol separates between one test and another
    Each test is parsed using the helper function above

    Args:
        tests_line (str): test line textual representation

    Returns:
        TestsType: a TestsType instance that represent the given data
    """
    tests_type, tests = _get_test_line_info(tests_line)
    tests = [_parse_test(test) for test in tests if test]
    return TEST_TYPES[tests_type](*tests)


def parse_fingerprint(fingerprint_lines: List[str]) -> FingerPrint:
    """
    Parses a fingerprint textual representation into a common.Fingerprint instance
    The fingerprint's description is extracted from the first line: "Fingerprint <description>
    Than each line should be a test line that is parsed with the help of the helpers functions above

    Args:
        fingerprint_lines (List[str]): List of a single clean fingerprint lines

    Returns:
        FingerPrint: a Fingerprint instance that represent the given data
    """
    description_line = fingerprint_lines[0]
    assert description_line.startswith("Fingerprint ")
    description = description_line[len("Fingerprint "):-1]
    tests_types = [_parse_tests_line(tests_line) for tests_line in fingerprint_lines[1:]]

    # Broken nmap fingerprint is missing an IE test line, lets fix it by adding an empty one
    if description == "FreeNAS 9.10 (FreeBSD 10.3-STABLE)":
        tests_types.append(TEST_TYPES["IE"]())

    return FingerPrint(*tests_types, description)


def parse_tests_points(fingerprint_lines: List[str]) -> TestsPoints:
    """
    Parses test points in a form of a textual fingerprint into a common.TestsPoints instance.
    The format of the test points fingerprint is similar to a regular fingerprint but every "test result" is a decimal
    number that defines the number of points for the test

    Args:
        fingerprint_lines (List[str]): List of a single clean fingerprint lines

    Returns:
        TestsPoints: a TestsPoints instance that represent the given data
    """
    assert fingerprint_lines[0].startswith(TestsPoints.PREFIX)
    tests_points: Dict[str, Dict[str, int]] = {}
    # for each test line
    for tests_line in fingerprint_lines[1:]:
        tests_type, tests = _get_test_line_info(tests_line)
        # create a mapping of points for each test in the current tests line
        tests_points[tests_type] = {}
        for test in tests:
            test_name, test_points = test.split(TEST_NAME_RESULT_SEP)
            # set test points
            tests_points[tests_type][test_name] = int(test_points)
    return TestsPoints(tests_points)
