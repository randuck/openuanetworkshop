import logging
from pathlib import Path

from osscan.common.fingerprint import TestsPoints

from .fingerprint import parse_fingerprint, parse_tests_points

# TODO: maybe in future its better to search for an installed nmap's db and fallback to using our own resource
NMAP_OS_DB_PATH = str(Path(__file__).parent / "nmap-os-db")


class FingerPrintDB:
    """
    Manages parsing and interaction with nmap_os_db file
    """
    PREFIXES_TO_IGNORE = ["#", "Class", "CPE"]
    EOF = "\n"  # End Of Fingerprint

    def __init__(self, db_path=NMAP_OS_DB_PATH):
        self.tests_points = {}
        self.db_path = db_path
        self.db_fd = None

    def __iter__(self):
        """
        Initialize a generator that will be used to iterate every fingerprint in the given database file
        """
        self.fingerprint_generator = self._generate_fingerprint()
        # reuse already opened db file
        if not self.db_fd or self.db_fd.closed:
            self.db_fd = open(self.db_path, "r", encoding="Latin1")
        # reset to beginning
        self.db_fd.seek(0)
        # parse test points fingerprint (which is always the first one)
        self._parse_tests_points()
        return self

    def __next__(self):
        return next(self.fingerprint_generator)

    def _parse_tests_points(self):
        """
        Read the test points fingerprint and return a parsed TestsPoints
        Note: expected to be executed at the beginning of the db because test point fingerprint is the first one.
        """
        tests_points_fingerprint_lines = []
        # search for test point fingerprint prefix ("MatchPoints")
        for line in self.db_fd:
            if line.startswith(TestsPoints.PREFIX):
                tests_points_fingerprint_lines.append(line)
                break

        # read fingerprint lines until EOF
        while ((line := next(self.db_fd)) != self.EOF):
            tests_points_fingerprint_lines.append(line)
        self.tests_points = parse_tests_points(tests_points_fingerprint_lines)

    def _generate_fingerprint(self):
        """
        A generator that reads the next fingerprint from the database file and yield a parsed FingerPrint instance
        """
        current_fingerprint_lines = []
        for line in self.db_fd:
            if any(line.startswith(prefix) for prefix in self.PREFIXES_TO_IGNORE):
                # skip non relevant lines (Comments, Class & CPE lines)
                continue

            if line == self.EOF:
                # Finished reading a fingerprint (found end of fingerprint)
                if current_fingerprint_lines:
                    try:
                        yield parse_fingerprint(current_fingerprint_lines)
                    except:
                        description = current_fingerprint_lines[0][len("Fingerprint "):-1]
                        logging.debug("Failed parsing fingerprint of: " + description)
                    current_fingerprint_lines = []
            else:
                current_fingerprint_lines.append(line)

        self.db_fd.close()
