import unittest

from osscan.common.fingerprint import (TEST_TYPES, FingerPrint, Test,
                                       TestsPoints)

from .fingerprint import (GreaterThan, LessThan, Or, Range, parse_fingerprint,
                          parse_test_result, parse_tests_points)
from .fingerprint_db import FingerPrintDB

TEST_POINTS_EXAMPLE = """MatchPoints
SEQ(SP=25%GCD=75%ISR=25%TI=100%CI=50%II=100%SS=80%TS=100)
OPS(O1=20%O2=20%O3=20%O4=20%O5=20%O6=20)
WIN(W1=15%W2=15%W3=15%W4=15%W5=15%W6=15)
ECN(R=100%DF=20%T=15%TG=15%W=15%O=15%CC=100%Q=20)
T1(R=100%DF=20%T=15%TG=15%S=20%A=20%F=30%RD=20%Q=20)
T2(R=80%DF=20%T=15%TG=15%W=25%S=20%A=20%F=30%O=10%RD=20%Q=20)
T3(R=80%DF=20%T=15%TG=15%W=25%S=20%A=20%F=30%O=10%RD=20%Q=20)
T4(R=100%DF=20%T=15%TG=15%W=25%S=20%A=20%F=30%O=10%RD=20%Q=20)
T5(R=100%DF=20%T=15%TG=15%W=25%S=20%A=20%F=30%O=10%RD=20%Q=20)
T6(R=100%DF=20%T=15%TG=15%W=25%S=20%A=20%F=30%O=10%RD=20%Q=20)
T7(R=80%DF=20%T=15%TG=15%W=25%S=20%A=20%F=30%O=10%RD=20%Q=20)
U1(R=50%DF=20%T=15%TG=15%IPL=100%UN=100%RIPL=100%RID=100%RIPCK=100%RUCK=100%RUD=100)
IE(R=50%DFI=40%T=15%TG=15%CD=100)"""

TEST_POINTS_EXPECTED_DICT = {
    "SEQ": {"SP": 25, "GCD": 75, "ISR": 25, "TI": 100, "CI": 50, "II": 100, "SS": 80, "TS": 100},
    "OPS": {"O1": 20, "O2": 20, "O3": 20, "O4": 20, "O5": 20, "O6": 20},
    "WIN": {"W1": 15, "W2": 15, "W3": 15, "W4": 15, "W5": 15, "W6": 15},
    "ECN": {"R": 100, "DF": 20, "T": 15, "TG": 15, "W": 15, "O": 15, "CC": 100, "Q": 20},
    "T1": {"R": 100, "DF": 20, "T": 15, "TG": 15, "S": 20, "A": 20, "F": 30, "RD": 20, "Q": 20},
    "T2": {"R": 80, "DF": 20, "T": 15, "TG": 15, "W": 25, "S": 20, "A": 20, "F": 30, "O": 10, "RD": 20, "Q": 20},
    "T3": {"R": 80, "DF": 20, "T": 15, "TG": 15, "W": 25, "S": 20, "A": 20, "F": 30, "O": 10, "RD": 20, "Q": 20},
    "T4": {"R": 100, "DF": 20, "T": 15, "TG": 15, "W": 25, "S": 20, "A": 20, "F": 30, "O": 10, "RD": 20, "Q": 20},
    "T5": {"R": 100, "DF": 20, "T": 15, "TG": 15, "W": 25, "S": 20, "A": 20, "F": 30, "O": 10, "RD": 20, "Q": 20},
    "T6": {"R": 100, "DF": 20, "T": 15, "TG": 15, "W": 25, "S": 20, "A": 20, "F": 30, "O": 10, "RD": 20, "Q": 20},
    "T7": {"R": 80, "DF": 20, "T": 15, "TG": 15, "W": 25, "S": 20, "A": 20, "F": 30, "O": 10, "RD": 20, "Q": 20},
    "U1": {"R": 50, "DF": 20, "T": 15, "TG": 15, "IPL": 100, "UN": 100, "RIPL": 100, "RID": 100, "RIPCK": 100, "RUCK": 100, "RUD": 100},
    "IE": {"R": 50, "DFI": 40, "T": 15, "TG": 15, "CD": 100}
}

FINGERPRINT_EXAMPLE_DESCRIPTION = "Microsoft Windows 10 1703"

# Version 1703 (OS Build 15063.296)
FINGERPRINT_EXAMPLE = f"""Fingerprint {FINGERPRINT_EXAMPLE_DESCRIPTION}
SEQ(SP=FE-108%GCD=1-6%ISR=108-112%TI=I%CI=I%II=I%TS=A)
OPS(O1=MFFD7ST11%O2=MFFD7ST11%O3=MFFD7NNT11%O4=MFFD7ST11%O5=MFFD7ST11%O6=MFFD7ST11)
WIN(W1=FE88%W2=FED4%W3=FF20%W4=FFDC%W5=FFDC%W6=FFDC)
ECN(R=Y%DF=Y%T=7B-85%TG=80%W=FAF0%O=MFFD7NNS%CC=Y%Q=)
T1(R=Y%DF=Y%T=7B-85%TG=80%S=O%A=S+%F=AS%RD=0%Q=)
T2(R=Y%DF=Y%T=7B-85%TG=80%W=0%S=Z%A=S%F=AR%O=%RD=0%Q=)
T3(R=Y%DF=Y%T=7B-85%TG=80%W=0%S=Z%A=O%F=AR%O=%RD=0%Q=)
T4(R=Y%DF=Y%T=7B-85%TG=80%W=0%S=A%A=O%F=R%O=%RD=0%Q=)
T5(R=Y%DF=Y%T=7B-85%TG=80%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)
T6(R=Y%DF=Y%T=7B-85%TG=80%W=0%S=A%A=O%F=R%O=%RD=0%Q=)
T7(R=Y%DF=Y%T=7B-85%TG=80%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)
U1(DF=N%T=7B-85%TG=80%IPL=164%UN=0%RIPL=G%RID=G%RIPCK=Z%RUCK=G%RUD=G)
IE(DFI=N%T=7B-85%TG=80%CD=Z)"""

FINGERPRINT_EXAMPLE_DICT = {
    "SEQ": {"SP": "FE-108", "GCD": "1-6", "ISR": "108-112", "TI": "I", "CI": "I", "II": "I", "TS": "A"},
    "OPS": {"O1": "MFFD7ST11", "O2": "MFFD7ST11", "O3": "MFFD7NNT11", "O4": "MFFD7ST11", "O5": "MFFD7ST11", "O6": "MFFD7ST11"},
    "WIN": {"W1": "FE88", "W2": "FED4", "W3": "FF20", "W4": "FFDC", "W5": "FFDC", "W6": "FFDC"},
    "ECN": {"R": "Y", "DF": "Y", "T": "7B-85", "TG": "80", "W": "FAF0", "O": "MFFD7NNS", "CC": "Y", "Q": ""},
    "T1": {"R": "Y", "DF": "Y", "T": "7B-85", "TG": "80", "S": "O", "A": "S+", "F": "AS", "RD": "0", "Q": ""},
    "T2": {"R": "Y", "DF": "Y", "T": "7B-85", "TG": "80", "W": "0", "S": "Z", "A": "S", "F": "AR", "O": "", "RD": "0", "Q": ""},
    "T3": {"R": "Y", "DF": "Y", "T": "7B-85", "TG": "80", "W": "0", "S": "Z", "A": "O", "F": "AR", "O": "", "RD": "0", "Q": ""},
    "T4": {"R": "Y", "DF": "Y", "T": "7B-85", "TG": "80", "W": "0", "S": "A", "A": "O", "F": "R", "O": "", "RD": "0", "Q": ""},
    "T5": {"R": "Y", "DF": "Y", "T": "7B-85", "TG": "80", "W": "0", "S": "Z", "A": "S+", "F": "AR", "O": "", "RD": "0", "Q": ""},
    "T6": {"R": "Y", "DF": "Y", "T": "7B-85", "TG": "80", "W": "0", "S": "A", "A": "O", "F": "R", "O": "", "RD": "0", "Q": ""},
    "T7": {"R": "Y", "DF": "Y", "T": "7B-85", "TG": "80", "W": "0", "S": "Z", "A": "S+", "F": "AR", "O": "", "RD": "0", "Q": ""},
    "U1": {"DF": "N", "T": "7B-85", "TG": "80", "IPL": "164", "UN": "0", "RIPL": "G", "RID": "G", "RIPCK": "Z", "RUCK": "G", "RUD": "G"},
    "IE": {"DFI": "N", "T": "7B-85", "TG": "80", "CD": "Z"}
}


FINGERPRINT_EXAMPLE_INSTANCE = FingerPrint(
    TEST_TYPES["SEQ"](Test("SP", Range(0xFE, 0x108)), Test("GCD", Range(0x1, 0x6)), Test("ISR", Range(0x108, 0x112)),
                      Test("TI", "I"), Test("CI", "I"), Test("II", "I"), Test("TS", "A")),
    TEST_TYPES["OPS"](Test("O1", "MFFD7ST11"), Test("O2", "MFFD7ST11"), Test("O3", "MFFD7NNT11"),
                      Test("O4", "MFFD7ST11"), Test("O5", "MFFD7ST11"), Test("O6", "MFFD7ST11")),
    TEST_TYPES["WIN"](Test("W1", "FE88"), Test("W2", "FED4"), Test("W3", "FF20"),
                      Test("W4", "FFDC"), Test("W5", "FFDC"), Test("W6", "FFDC")),
    TEST_TYPES["ECN"](Test("R", True), Test("DF", True), Test("T", Range(0x7B, 0x85)), Test("TG", "80"),
                      Test("W", "FAF0"), Test("O", "MFFD7NNS"), Test("CC", True), Test("Q", "")),
    TEST_TYPES["T1"](Test("R", True), Test("DF", True), Test("T", Range(0x7B, 0x85)), Test("TG", "80"),
                     Test("S", "O"), Test("A", "S+"), Test("F", "AS"), Test("RD", "0"), Test("Q", "")),
    TEST_TYPES["T2"](Test("R", True), Test("DF", True), Test("T", Range(0x7B, 0x85)), Test("TG", "80"), Test("W", "0"),
                     Test("S", "Z"), Test("A", "S"), Test("F", "AR"), Test("O", ""), Test("RD", "0"), Test("Q", "")),
    TEST_TYPES["T3"](Test("R", True), Test("DF", True), Test("T", Range(0x7B, 0x85)), Test("TG", "80"), Test("W", "0"),
                     Test("S", "Z"), Test("A", "O"), Test("F", "AR"), Test("O", ""), Test("RD", "0"), Test("Q", "")),
    TEST_TYPES["T4"](Test("R", True), Test("DF", True), Test("T", Range(0x7B, 0x85)), Test("TG", "80"), Test("W", "0"),
                     Test("S", "A"), Test("A", "O"), Test("F", "R"), Test("O", ""), Test("RD", "0"), Test("Q", "")),
    TEST_TYPES["T5"](Test("R", True), Test("DF", True), Test("T", Range(0x7B, 0x85)), Test("TG", "80"), Test("W", "0"),
                     Test("S", "Z"), Test("A", "S+"), Test("F", "AR"), Test("O", ""), Test("RD", "0"), Test("Q", "")),
    TEST_TYPES["T6"](Test("R", True), Test("DF", True), Test("T", Range(0x7B, 0x85)), Test("TG", "80"), Test("W", "0"),
                     Test("S", "A"), Test("A", "O"), Test("F", "R"), Test("O", ""), Test("RD", "0"), Test("Q", "")),
    TEST_TYPES["T7"](Test("R", True), Test("DF", True), Test("T", Range(0x7B, 0x85)), Test("TG", "80"), Test("W", "0"),
                     Test("S", "Z"), Test("A", "S+"), Test("F", "AR"), Test("O", ""), Test("RD", "0"), Test("Q", "")),
    TEST_TYPES["U1"](Test("DF", False), Test("T", Range(0x7B, 0x85)), Test("TG", "80"), Test("IPL", "164"), Test("UN", "0"),
                     Test("RIPL", "G"), Test("RID", "G"), Test("RIPCK", "Z"), Test("RUCK", "G"), Test("RUD", "G")),
    TEST_TYPES["IE"](Test("DFI", False), Test("T", Range(0x7B, 0x85)), Test("TG", "80"), Test("CD", "Z")),
    description=FINGERPRINT_EXAMPLE_DESCRIPTION
)


class NmapOsDbParserTests(unittest.TestCase):
    """
    Unittests for the nmap os db parsing package
    """

    def test_result_value_range(self):
        res = parse_test_result("9E-A8")
        # validate parsing
        self.assertEqual(res, Range(0x9E, 0xA8))
        # make sure matches against a numeric values which falls within the range
        self.assertEqual(res, 0x9E)
        self.assertEqual(res, 0x9E + (0xA8 - 0x9E) // 2)
        self.assertEqual(res, 0xA8)
        # make sure don't match against a numeric values which falls outside the range
        self.assertNotEqual(res, 0xA8 + 1)
        self.assertNotEqual(res, 0x9E - 1)

    def test_result_value_greater_than(self):
        res = parse_test_result(">B00000")
        # validate parsing
        self.assertEqual(res, GreaterThan(0xB00000))
        # make sure matches against greater values
        self.assertEqual(res, 0xB00000 + 1)
        # make sure don't match against greater values
        self.assertNotEqual(res, 0xB00000)
        self.assertNotEqual(res, 0)

    def test_result_value_less_than(self):
        res = parse_test_result("<B00000")
        # validate parsing
        self.assertEqual(res, LessThan(0xB00000))
        # make sure matches against greater values
        self.assertEqual(res, 0xB00000 - 1)
        # make sure don't match against greater values
        self.assertNotEqual(res, 0xB00000)
        self.assertNotEqual(res, 0xFFFFFF)

    def test_result_simple_ored_values(self):
        res = parse_test_result("3200|6400|9600|C800|FA00")

        # validate parsing
        self.assertEqual(res, Or(["3200", "6400", "9600", "C800", "FA00"]))
        # make sure matches any of the options
        self.assertEqual(res, parse_test_result(0x3200))
        self.assertEqual(res, parse_test_result(0x6400))
        self.assertEqual(res, parse_test_result(0x9600))
        self.assertEqual(res, "C800")
        self.assertEqual(res, "FA00")

    def test_result_complex_ored_values(self):
        res = parse_test_result(">B00000|D2-DC|FFFF|S+")
        # validate parsing
        self.assertEqual(res, Or([GreaterThan(0xB00000), Range(0xD2, 0xDC), parse_test_result(0xFFFF), "S+"]))
        # make sure matches any of the options
        self.assertEqual(res, GreaterThan(0xB00000))
        self.assertEqual(res, 0xB00001)
        self.assertEqual(res, Range(0xD2, 0xDC))
        self.assertEqual(res, 0xD2 + (0xDC - 0xD2) // 2)
        self.assertEqual(res, "FFFF")
        self.assertEqual(res, "S+")
        # make sure don't match
        self.assertNotEqual(res, 0xB00000)
        self.assertNotEqual(res, 0xD1)
        self.assertNotEqual(res, "AAAA")
        self.assertNotEqual(res, "A+")

    def test_points_fingerprint(self):
        res = parse_tests_points(TEST_POINTS_EXAMPLE.split("\n"))
        # validate parsing
        self.assertEqual(res, TestsPoints(TEST_POINTS_EXPECTED_DICT))

        # check indexing
        self.assertEqual(res['SEQ', 'GCD'], 75)
        # check invalid indexing
        with self.assertRaises(KeyError):
            res['NotATestLine']
        with self.assertRaises(KeyError):
            res['SEQ', 'NotATest']
        with self.assertRaises(IndexError):
            res[8]

    def test_single_fingerprint_from_dict(self):
        res = parse_fingerprint(FINGERPRINT_EXAMPLE.split('\n'))
        # validate parsing
        self.assertEqual(res, FingerPrint.from_dict(FINGERPRINT_EXAMPLE_DICT,
                         description=FINGERPRINT_EXAMPLE_DESCRIPTION))

    def test_single_fingerprint(self):
        res = parse_fingerprint(FINGERPRINT_EXAMPLE.split('\n'))
        # validate parsing
        self.assertEqual(res, FINGERPRINT_EXAMPLE_INSTANCE)

    def test_all_fingerprints(self):
        db = FingerPrintDB()
        for _fingerprint in db:
            # FingerPrintDB's iterator parse all nmap db's fingerprints one by one
            pass


if __name__ == "__main__":
    unittest.main()
