import logging
from abc import ABC, abstractmethod
from random import getrandbits, randint
from typing import Any

from scapy.layers.inet import ICMP, IP, TCP, UDP
from scapy.packet import Raw
from scapy.sendrecv import SndRcvList, _PacketIterable, sr

from osscan.common.types import TargetData


class ProbeFailedException(Exception):
    pass


class Probe(ABC):
    """
    Abstract class that defines probe format
    """
    probed_data = None
    DEFAULT_TIMEOUT = 3

    @property
    def name(self):
        assert self.__class__.__name__.endswith("Probe")
        # class name without the Probe postfix in uppercase
        return self.__class__.__name__[:-5].upper()

    @abstractmethod
    def send(self, target: TargetData) -> None:
        """
        Create probe packet and finally send them using `_send_packets`
        """
        raise NotImplementedError

    def _set_packets_source_port(self, probe_packets: _PacketIterable):
        """
        Set probe packets source port in an incremental fashion
        """
        base_sport = randint(1024, 2**16-1 - len(probe_packets))
        for packet in probe_packets:
            if packet.haslayer(TCP):
                packet[TCP].sport = base_sport
                base_sport += 1
            elif packet.haslayer(UDP):
                packet[UDP].sport = base_sport
                base_sport += 1

    def _set_answered_probes_numbers(self, probe_packets: _PacketIterable, answered_probes: SndRcvList):
        """
        Add an index to the answered packets query that corresponds to the original sent packet order
        """
        for qa in answered_probes:
            for (probe_index, packet) in enumerate(probe_packets):
                if packet == qa.query:
                    setattr(qa.query, "probe_number", probe_index + 1)

    def _send_packets(self, target: TargetData, packets: _PacketIterable, **kwargs: Any) -> None:
        logging.info(f"Initiating {self.name} probes [{len(packets)} packets]")

        kwargs.setdefault("timeout", self.DEFAULT_TIMEOUT)
        self._set_packets_source_port(packets)
        ans, unans = sr(packets, iface=target.iface, verbose=False, **kwargs)

        logging.debug(f"Probe results: {ans}, {unans}")
        if not ans:
            logging.error(f"Target didn't response to any of the the {self.name} probes T_T")
        self._set_answered_probes_numbers(packets, ans)
        self.probed_data = ans

    def dump(self) -> dict:
        """
        probe response data by probe name
        """
        return {self.name: self.probed_data}


def get_random_u16() -> int:
    return getrandbits(16)


def get_random_u32() -> int:
    return getrandbits(32)


class SeqProbe(Probe):
    """
    Probe using a series of six TCP SYN packets (100ms apart) which vary in the TCP options they use
    and the TCP window field value. The following list provides the options and values for all six packets:
    #1: window scale (10), NOP, MSS (1460), timestamp (TSval:0xFFFFFFFF; TSecr: 0), SACK permitted. window field is 1.
    #2: MSS (1400), window scale (0), SACK permitted, timestamp (TSval: 0xFFFFFFFF; TSecr: 0), EOL. window field is 63.
    #3: Timestamp (TSval: 0xFFFFFFFF; TSecr: 0), NOP, NOP, window scale (5), NOP, MSS (640).        window field is 4.
    #4: SACK permitted, Timestamp (TSval: 0xFFFFFFFF; TSecr: 0), window scale (10), EOL.            window field is 4.
    #5: MSS (536), SACK permitted, Timestamp (TSval: 0xFFFFFFFF; TSecr: 0), window scale (10), EOL. window field is 16.
    #6: MSS (265), SACK permitted, Timestamp (TSval: 0xFFFFFFFF; TSecr: 0).                         window field is 512.
    For more information see: https://nmap.org/book/osdetect-methods.html#osdetect-probes-seq
    """
    SEQ_TCP_OPTIONS = [
        [("WScale", 10), ("NOP", ''), ("MSS", 1460), ("Timestamp", (0xFFFFFFFF, 0)), ("SAckOK", '')],
        [("MSS", 1400), ("WScale", 0), ("SAckOK", ''), ("Timestamp", (0xFFFFFFFF, 0)), ("EOL", 0)],
        [("Timestamp", (0xFFFFFFFF, 0)), ("NOP", ''), ("NOP", ''), ("WScale", 5), ("NOP", ''), ("MSS", 640)],
        [("SAckOK", ''), ("Timestamp", (0xFFFFFFFF, 0)), ("WScale", 10), ("EOL", 0)],
        [("MSS", 536), ("SAckOK", ''), ("Timestamp", (0xFFFFFFFF, 0)), ("WScale", 10), ("EOL", 0)],
        [("MSS", 265), ("SAckOK", ''), ("Timestamp", (0xFFFFFFFF, 0))],
    ]
    WINDOW_SIZES = [1, 63, 4, 4, 16, 512]
    PROBE_INTERVAL_S = 0.1

    def send(self, target: TargetData) -> None:
        rand_seq = get_random_u32()  # 32 bits field
        rand_ack = get_random_u32()  # 32 bits field
        probes = [
            IP(dst=target.ip_address) /
            TCP(dport=target.open_tcp_port, seq=rand_seq + i, ack=rand_ack,
                window=self.WINDOW_SIZES[i], options=self.SEQ_TCP_OPTIONS[i])
            for i in range(len(self.SEQ_TCP_OPTIONS))
        ]
        self._send_packets(target, probes, inter=self.PROBE_INTERVAL_S)


class IEProbe(Probe):
    """
    The IE test involves sending two ICMP echo request packets to the target. The first one has the IP DF bit set,
    a type-of-service (TOS) byte value of zero, a code of nine (even though it should be zero), the sequence number 295,
    a random IP ID and ICMP request identifier, and 120 bytes of 0x00 for the data payload.
    The second ping query is similar, except a TOS of four (IP_TOS_RELIABILITY) is used, the code is zero,
    150 bytes of data is sent, and the ICMP request ID and sequence numbers are incremented by one from the previous
    query values.
    For more information see: https://nmap.org/book/osdetect-methods.html#idm45323735500048
    """

    def send(self, target: TargetData) -> None:
        def create_ie_probe(ipid: int, tos: int, code: int, seq: int, data: bytes):
            return IP(dst=target.ip_address, id=ipid, flags='DF', tos=tos) / ICMP(code=code, seq=seq) / Raw(load=data)

        rand_ipid = get_random_u16()  # 16 bit field
        ie_1 = create_ie_probe(ipid=rand_ipid, tos=0, code=9, seq=255, data=bytes(120))
        ie_2 = create_ie_probe(ipid=rand_ipid+1, tos=4, code=0, seq=ie_1[ICMP].seq+1, data=bytes(150))
        self._send_packets(target, packets=[ie_1, ie_2])


class EcnProbe(Probe):
    """
    This probe tests for explicit congestion notification (ECN) support in the target TCP stack.
    ECN is a method for improving Internet performance by allowing routers to signal congestion problems before they
    start having to drop packets. It is documented in RFC 3168. Like nmap we test this by sending a SYN packet which
    also has the ECN CWR and ECE congestion control flags set. For an unrelated (to ECN) test, the urgent field value of
    0xF7F5 is used even though the urgent flag is not set. The acknowledgment number is zero, sequence number is random,
    window size field is three, and the reserved bit which immediately precedes the CWR bit is set.
    TCP options are WScale (10), NOP, MSS (1460), SACK permitted, NOP, NOP.
    The probe is sent to an open port.
    For more information see: https://nmap.org/book/osdetect-methods.html#idm45323735480704
    """

    def send(self, target: TargetData):
        rand_seq = get_random_u32()  # 32 bits field
        ecn = IP(dst=target.ip_address) / TCP(dport=target.open_tcp_port, flags="SEC", ack=0, seq=rand_seq, urgptr=0xF7F5,
                                              options=[("WScale", 10),
                                                       ("NOP", ''), ("MSS", 1460), ("SAckOK", ''), ("NOP", ''), ("NOP", '')])
        self._send_packets(target, packets=[ecn])


class TcpProbe(Probe):
    """
    The six T2 through T7 tests each send one TCP probe packet. With one exception, the TCP options data in each case is
    (in hex) 03030A0102040109080AFFFFFFFF000000000402, the exception is that T7 uses a Window scale=15 rather than 10.
    The variable characteristics of each probe are described below:
    T2 sends a TCP null (no flags set) packet with the IP DF bit set and a window field of 128 to an open port.
    T3 sends a TCP packet with the SYN, FIN, URG, and PSH flags set and a window field of 256 to an open port. The IP DF bit is not set.
    T4 sends a TCP ACK packet with IP DF and a window field of 1024 to an open port.
    T5 sends a TCP SYN packet without IP DF and a window field of 31337 to a closed port.
    T6 sends a TCP ACK packet with IP DF and a window field of 32768 to a closed port.
    T7 sends a TCP packet with the FIN, PSH, and URG flags set and a window field of 65535 to a closed port. The IP DF bit is not set.
    For more information see: https://nmap.org/book/osdetect-methods.html#osdetect-probes-t
    """
    COMMON_TCP_OPTIONS = [("WScale", 10), ("NOP", ''), ("MSS", 265), ("Timestamp", (0xFFFFFFFF, 0)), ("SAckOK", '')]
    T7_TCP_OPTIONS = [("WScale", 15)] + COMMON_TCP_OPTIONS[1:]

    def send(self, target: TargetData) -> None:
        def _create_tcp_probe(use_open_port: bool, seq: int, ack: int, window: int, tcp_options: str,
                              ip_flags: str = "", tcp_flags: str = "S") -> IP:
            tcp_port = target.open_tcp_port if use_open_port else target.closed_tcp_port
            return IP(dst=target.ip_address, flags=ip_flags) / TCP(dport=tcp_port, seq=seq, ack=ack, window=window,
                                                                   options=tcp_options, flags=tcp_flags)

        rand_seq = get_random_u32()  # 32 bits field
        rand_ack = get_random_u32()  # 32 bits field
        t_2 = _create_tcp_probe(use_open_port=True, seq=rand_seq, ack=0, window=128,
                                tcp_options=self.COMMON_TCP_OPTIONS, ip_flags='DF', tcp_flags="")
        t_3 = _create_tcp_probe(use_open_port=True, seq=rand_seq, ack=rand_ack, window=256,
                                tcp_options=self.COMMON_TCP_OPTIONS, tcp_flags="SFUP")
        t_4 = _create_tcp_probe(use_open_port=True, seq=rand_seq, ack=rand_ack, window=1024,
                                tcp_options=self.COMMON_TCP_OPTIONS, ip_flags='DF', tcp_flags="A")
        t_5 = _create_tcp_probe(use_open_port=False, seq=rand_seq, ack=rand_ack,
                                window=31337, tcp_options=self.COMMON_TCP_OPTIONS)
        t_6 = _create_tcp_probe(use_open_port=False, seq=rand_seq, ack=rand_ack, window=32768,
                                ip_flags='DF', tcp_options=self.COMMON_TCP_OPTIONS, tcp_flags="A")
        t_7 = _create_tcp_probe(use_open_port=False, seq=rand_seq, ack=0, window=65535,
                                tcp_options=self.T7_TCP_OPTIONS, tcp_flags='FUP')

        t_probes = [t_2, t_3, t_4, t_5, t_6, t_7]
        self._send_packets(target, packets=t_probes)

        # fill unanswered tcp probes with None
        ordered_probe_data = [None] * len(t_probes)
        for probe in self.probed_data:
            ordered_probe_data[probe.query.probe_number - 1] = probe
        self.probed_data = ordered_probe_data


class U1Probe(Probe):
    """
    This probe is a UDP packet sent to a closed port.
    The character ‘C’ (0x43) is repeated 300 times for the data field.
    The IP ID value is set to 0x1042 for operating systems which allow us to set this. If the port is truly closed
    and there is no firewall in place, Nmap expects to receive an ICMP port unreachable message in return.
    For more information see: https://nmap.org/book/osdetect-methods.html#idm45323735431376
    """

    def send(self, target: TargetData):
        my_ttl = 51 + randint(0, 13)
        u1 = IP(dst=target.ip_address, id=0x1042, ttl=my_ttl) / UDP(dport=target.closed_udp_port) / Raw(load=b'C' * 300)
        self._send_packets(target, packets=[u1])


all_probes = Probe.__subclasses__()


def probe_target(target_data: TargetData) -> dict:
    """
    Probe given target using all of the probes which inherit from Probe
    """
    output = {}
    for probe_cls in all_probes:
        probe = probe_cls()
        probe.send(target_data)
        output.update(probe.dump())
    return output
