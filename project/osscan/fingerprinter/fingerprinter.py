import functools
import logging
from typing import Any, List, Mapping

from scapy.layers.inet import ICMP, IP
from scapy.sendrecv import QueryAnswer, SndRcvList

from osscan.common.fingerprint import SUPPORTED_TESTS, FingerPrint, Test
from osscan.common.types import TargetData

from . import common_tests as common
from . import icmp_tests as icmp
from . import seq_tests as seq
from . import tcp_tests as tcp
from . import udp_tests as udp
from .probes import probe_target


def test_line(tests_type_name: str):
    """
    Decorator which conveniently add tests dict as a test line under the given name in the tests_results attribute
    Note: the name of the test line must be supported (in SUPPORTED_TESTS)
    For example:
    ...: @test_line("BEST_TEST")
    ...: def do_stuff(self):
    ...:     return {'test1': 1, 'test2': 2}
    Calling the `do_stuff` method (incase "BEST_TEST" is supported) will update the tests_results as follows:
    ...: f = FingerPrint(target_data)
    ...: f.do_stuff()
    ...: print(f.tests_result()) # {"BEST_TEST": {'test1': 1, 'test2': 2}, ...}
    """
    def decorator(func):
        @functools.wraps(func)
        def wrapper(self, *args, **kwargs) -> Mapping[str, Any]:
            if tests_type_name not in self.tests_results:
                raise ValueError(f"Cannot create a testline of an unsupported test type: {tests_type_name}")
            logging.debug(f"Running {tests_type_name} tests")
            tests_dict = func(self, *args, **kwargs)
            for (test_name, test_result) in tests_dict.items():
                self._set_test_result(tests_type_name, test_name, test_result)
            return tests_dict
        return wrapper
    return decorator


def test_lines(tests_types_names: List[str]):
    """
    Like test line but supported multiple test lines.
    the decorated test function is expected to return a list of dicts. each dict should corespond to a test type name in given list
    """
    def decorator(func):
        @functools.wraps(func)
        def wrapper(self, *args, **kwargs) -> Mapping[str, Any]:
            for tests_type_name in tests_types_names:
                if tests_type_name not in self.tests_results:
                    raise ValueError(f"Cannot create a testline of an unsupported test types: {tests_type_name}")
            tests_dicts = func(self, *args, **kwargs)
            for (tests_type_name, tests_dict) in zip(tests_types_names, tests_dicts):
                for (test_name, test_result) in tests_dict.items():
                    self._set_test_result(tests_type_name, test_name, test_result)
            return tests_dicts
        return wrapper
    return decorator


class FingerPrinter:
    """
    Responsible for creating a fingerprint of a target
    """

    def __init__(self, target_data: TargetData):
        self.target_data = target_data
        self.tests_results = {test_type: {} for test_type in SUPPORTED_TESTS}

    def _set_test_result(self, tests_type_name, test_name, test_result):
        self.tests_results[tests_type_name][test_name] = test_result

    @test_line("SEQ")
    def _run_seq_tests(self, probes_data: Mapping[str, SndRcvList]) -> Mapping[str, Any]:
        """
        Run seq tests on the seq probes in the following order: <SP, GCD, ISR, TI, CI, II, SS, TS>
        in order to produce the SEQ test line
        """
        test_result = {}
        test_info = seq.preprocess_probes_into_seq_info(probes_data)
        if len(probes_data["SEQ"]) >= 4:
            # (SP, GCD, ISR)
            # calculate greatest common division of the ipid differences (GCD)
            seq_gcd = seq.run_gcd_test(test_info)
            # set "SP" and "ISR" tests to zero by default
            test_result.update({"SP": 0, "GCD": seq_gcd, "ISR": 0})
            if seq_gcd > 0:
                test_result["ISR"] = seq.run_isr_test(test_info)
                test_result["SP"] = seq.run_sp_test(seq_gcd, test_info)

        # (TI, CI, II)
        tcp_ipid_class = seq.run_ipid_test(test_info.seq_ipids, 3)
        icmp_ipid_class = seq.run_ipid_test(test_info.icmp_ipids, 2)
        test_result.update({"TI": tcp_ipid_class, "II": icmp_ipid_class,
                            "CI": seq.run_ipid_test(test_info.closed_ipids, 2)})

        # (SS, TS)
        test_result["SS"] = seq.run_ss_test(test_info, tcp_ipid_class, icmp_ipid_class)
        test_result["TS"] = seq.run_ts_test(test_info)
        return test_result

    @test_line("OPS")
    def _run_ops_tests(self, probes_data: SndRcvList) -> Mapping[str, Any]:
        """
        Run ops tests on the seq probes in the following order: <O1, O2, O3, O4, O5, O6>
        in order to produce the OPS test line
        """
        # tcp options string representation for each ith answered probe query under O{i}
        return {f'O{query.probe_number}': common.run_o_test(answer) for query, answer in probes_data['SEQ']}

    @test_line("WIN")
    def _run_win_tests(self, probes_data: SndRcvList) -> Mapping[str, Any]:
        """
        Run win tests on the seq probes in the following order: <W1, W2, W3, W4, W5, W6>
        in order to produce the WIN test line
        """
        # tcp options string representation for each ith answered probe query under O{i}
        return {f'W{query.probe_number}': common.run_w_test(answer) for query, answer in probes_data['SEQ']}

    def _run_ttl_tests(self, probes_data: SndRcvList, probe_answer: IP, tests_result: Mapping[str, Any]):
        """
        If we got an answer to the U1 probe, try using the (T) test to examine distance to the target (how many hops).
        Otherwise, fallback to the (TG) test.
        Populate the given tests result dict with the result
        """
        tests_result['T'] = Test.OMIT
        if (probes_data['U1']):
            u1_query, u1_answer = probes_data['U1'][0]
            tests_result['T'] = common.run_t_test(probe_answer, u1_query, u1_answer)
        if tests_result['T'] == Test.OMIT:
            # fallback to guessing the ttl incase u1 probes wasn't answered or (T) test failed
            tests_result['TG'] = common.run_tg_test(probe_answer)

    @test_line("ECN")
    def _run_ecn_tests(self, probes_data: SndRcvList) -> Mapping[str, Any]:
        """
        Run ecn tests on the ecn probe in the following order: <R, DF, T, TG, W, O, CC, Q>
        in order to produce the ECN test line
        """
        tests_result = {"R": "N"}
        if not probes_data["ECN"]:
            return tests_result

        tests_result['R'] = "Y"
        ecn_answer = probes_data["ECN"][0].answer
        tests_result["DF"] = common.run_df_test(ecn_answer)
        self._run_ttl_tests(probes_data, ecn_answer, tests_result)
        tests_result["W"] = common.run_w_test(ecn_answer)
        tests_result["O"] = common.run_o_test(ecn_answer)
        tests_result["CC"] = tcp.run_cc_test(ecn_answer)
        tests_result["Q"] = common.run_q_test(ecn_answer)
        return tests_result

    def _run_t_tests(self, probes_data: SndRcvList, t_probe: QueryAnswer) -> Mapping[str, Any]:
        """
        Run tcp tests on a given tcp probe in the following order: <R, DF, T, TG, W, S, A, F, O, RD, Q>
        in order to produce the T{i} test line
        """
        tests_result = {"R": "N"}
        if not t_probe:
            return tests_result

        tests_result['R'] = "Y"
        t_query, t_answer = t_probe
        tests_result["DF"] = common.run_df_test(t_answer)
        self._run_ttl_tests(probes_data, t_answer, tests_result)
        tests_result["W"] = common.run_w_test(t_answer)
        tests_result["S"] = tcp.run_s_test(t_query, t_answer)
        tests_result["A"] = tcp.run_a_test(t_query, t_answer)
        tests_result["F"] = tcp.run_f_test(t_answer)
        tests_result["O"] = common.run_o_test(t_answer)
        tests_result["RD"] = tcp.run_rd_test(t_answer)
        tests_result["Q"] = common.run_q_test(t_answer)
        return tests_result

    @test_line("T1")
    def _run_t1_tests(self, probes_data: SndRcvList) -> Mapping[str, Any]:
        """
        Run the T tests on the seq probe in order to produce the T1 test line
        """
        seq_probe = None
        if len(probes_data['SEQ']) > 0:
            seq_probe = probes_data['SEQ'][0]
        tests_result = self._run_t_tests(probes_data, seq_probe)

        # Delete W, O tests as they are already exists in the OPS and WIN test lines
        tests_result.pop("W", None)
        tests_result.pop("O", None)

        return tests_result

    @test_lines(['T2', 'T3', 'T4', 'T5', 'T6', 'T7'])
    def _run_t2_to_t7_tests(self, probes_data: SndRcvList) -> List[Mapping[str, Any]]:
        """
        Run the T tests on the tcp probes in order to produce the T[2-7] test lines
        """
        tests_results = [self._run_t_tests(probes_data, probe) for probe in probes_data['TCP']]
        return tests_results

    @test_line('U1')
    def _run_u1_tests(self, probes_data: SndRcvList) -> Mapping[str, Any]:
        """
        Run udp tests on the udp probe in the following order: <DF, T, TG, IPL, UN, RIPL, RID, RIPCK, RUCK>
        in order to produce the U1 test line
        """
        tests_result = {"R": "N"}
        if not probes_data['U1']:
            return tests_result

        tests_result["R"] = "Y"
        u1_query, u1_answer = probes_data["U1"][0]
        # make sure the answer to the u1 probe answer is an ICMP unreachable as expected
        assert u1_answer.haslayer(ICMP) and u1_answer[ICMP].code == 3 and u1_answer[ICMP].type == 3
        tests_result["DF"] = common.run_df_test(u1_answer)
        self._run_ttl_tests(probes_data, u1_answer, tests_result)
        tests_result["IPL"] = udp.run_ipl_test(u1_answer)
        tests_result["UN"] = udp.run_un_test(u1_answer)
        tests_result["RIPL"] = udp.run_ripl_test(u1_answer)
        tests_result["RID"] = udp.run_rid_test(u1_query, u1_answer)
        tests_result["RIPCK"] = udp.run_ripck_test(u1_answer)
        tests_result["RUCK"] = udp.run_ruck_test(u1_query, u1_answer)
        tests_result["RUD"] = udp.run_rud_test(u1_answer)
        return tests_result

    @ test_line('IE')
    def _run_ie_tests(self, probes_data: SndRcvList) -> Mapping[str, Any]:
        """
        Run icmp tests on the icmp probes in the following order: <DFI, T, TG, CD>
        in order to produce the IE test line
        """
        tests_result = {"R": "N"}
        if len(probes_data['IE']) != 2:
            return tests_result

        tests_result["R"] = "Y"
        icmp1, icmp2 = probes_data['IE']
        tests_result["DFI"] = icmp.run_dfi_test(icmp1.answer, icmp2.answer)
        self._run_ttl_tests(probes_data, icmp1.answer, tests_result)
        tests_result["CD"] = icmp.run_cd_test(icmp1.answer, icmp2.answer)
        return tests_result

    def fingerprint(self) -> FingerPrint:
        """
        Create a fingerprint of a given target using a collection of tests which are ran on data
        that is retrieved from probing the target hosts
        """
        probes_data = probe_target(self.target_data)
        self._run_seq_tests(probes_data)
        self._run_ops_tests(probes_data)
        self._run_win_tests(probes_data)
        self._run_ecn_tests(probes_data)
        self._run_t1_tests(probes_data)
        self._run_t2_to_t7_tests(probes_data)
        self._run_u1_tests(probes_data)
        self._run_ie_tests(probes_data)
        return FingerPrint.from_dict(self.tests_results, description="Target FingerPrint")
