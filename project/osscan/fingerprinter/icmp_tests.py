"""
Contains tests that are relevant for icmp probes (So basically tests that are specifically for the IE test line)
"""

from typing import Union

from scapy.layers.inet import ICMP, IP

from . import utils
from .common_tests import run_df_test


def run_dfi_test(first_icmp_answer: IP, second_icmp_answer: IP) -> Union[int, str]:
    """
    Run the "Don't fragment (ICMP)" test (DFI)
    This is simply a modified version of the DF test that is used for the special IE probes.
    It compares results of the don't fragment bit for the two ICMP echo request probes sent.
    A value is returned according to the table at: https://nmap.org/book/osdetect-methods.html#osdetect-tbl-dfi
    """
    first_df = utils.decode_bool(run_df_test(first_icmp_answer))
    second_df = utils.decode_bool(run_df_test(second_icmp_answer))
    if first_df and second_df:
        return "Y"
    elif first_df and not second_df:
        return "S"
    elif not first_df and not second_df:
        return "N"
    return "O"


def run_cd_test(first_icmp_answer: IP, second_icmp_answer: IP) -> Union[str, int]:
    """
    Run the "ICMP response code" test (CD)
    The code value of an ICMP echo reply (type zero) packet is supposed to be zero.
    But some implementations wrongly send other values, particularly if the echo request has a nonzero code
    (as one of the IE tests does)
    A value is returned according to the table at: https://nmap.org/book/osdetect-methods.html#osdetect-tbl-cd
    """
    assert first_icmp_answer.haslayer(ICMP) and second_icmp_answer.haslayer(ICMP)
    first_code = first_icmp_answer[ICMP].code
    second_code = second_icmp_answer[ICMP].code

    if first_code == second_code:
        return first_code if first_code != 0 else "Z"
    if first_code == 9 and second_code == 0:
        # Same as the queries codes
        return "S"
    return "O"
