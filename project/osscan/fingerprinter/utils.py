import logging
from typing import Any, List

from scapy.layers.inet import IP, TCP

MAX_U32 = 0xFFFFFFFF


def mod_diff(x: int, y: int) -> int:
    """
    Minimum diff between two u32 numbers (including wrapping the 32-bit counter back to zero)
    """
    return min((x - y) % MAX_U32, (y - x) % MAX_U32)


def usec_diff(t1: float, t2: float) -> int:
    """
    Calculate difference between two given timestamps in microseconds
    Note that the given timestamps are treated as seconds

    Returns:
        the difference value if not-zero, otherwise 1 (to allow division by diff)
    """
    return int((t1 - t2) * 1000000) or 1


def get_tcp_option(packet: IP, tcp_option: str) -> Any:
    """
    Return the value that belongs to a given tcp option in a given packet.
    If the option is non-existent in packet's tcp option we return None
    """
    assert packet.haslayer(TCP)
    for (option, val) in packet[TCP].options:
        if option == tcp_option:
            return val
    return None


def get_tsval_option(packet: IP) -> int:
    """
    Return timestamp value of a given packet if the Timestamp tcp option exists, otherwise None
    """
    timestamp = get_tcp_option(packet, "Timestamp")
    if timestamp:
        ts_val, _ts_ecr = timestamp
        return ts_val
    return None


def is_ipid_class_incremental(ipid_class: str) -> bool:
    """
    Return wether the given ipid class is incremental
    (Which actually means one of the following possibilities ["I", "BI", "RI"])
    """
    return "I" in ipid_class


def filter_bad_ipids(ipids: List[int]):
    """
    clean ipid list from 0xffffffff values which aren't relevant for ipid tests
    """
    return [ipid for ipid in ipids if ipid != 0xffffffff]


def encode_bool(result: bool):
    """
    encode boolean value into fingerprint format: True => Y, False => N
    """
    return 'Y' if result else 'N'


def decode_bool(result: str):
    """
    decode boolean value from fingerprint format: Y => True, N => False
    """
    assert result in ['Y', 'N']
    return result == 'Y'
