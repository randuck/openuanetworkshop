"""
Contains all tcp tests **besides** those that are common or only relates to the SEQ tests
(So basically the non-common tcp tests the relates to ECN && T2-T7 test lines)
"""

from binascii import crc32
from typing import Union

from scapy.layers.inet import IP, TCP
from scapy.packet import Raw


def run_cc_test(ecn_probe_answer: IP) -> Union[str, bool]:
    """
    Run the "Explicit congestion notification" test (CC)
    return a value according to the CWR and ECE tcp flags in ecn probe response
    the value is returned according to the table at: https://nmap.org/book/osdetect-methods.html#osdetect-ecn
    """
    assert ecn_probe_answer.haslayer(TCP)
    is_ece = 'E' in ecn_probe_answer[TCP].flags
    is_cwr = 'C' in ecn_probe_answer[TCP].flags

    if not is_ece and not is_cwr:
        return "N"
    if is_ece and is_cwr:
        return 'S'
    return "Y" if is_cwr else 'O'


def run_s_test(probe_query: IP, probe_answer: IP):
    """
    Run the "TCP sequence number" test (S)
    Examines how the sequence number compares to the acknowledgment number from the probe query.
    A value is returned according to the table at: https://nmap.org/book/osdetect-methods.html#osdetect-tbl-seq
    """
    assert probe_query.haslayer(TCP) and probe_answer.haslayer(TCP)
    if probe_answer[TCP].seq == 0:
        return "Z"
    if probe_answer[TCP].seq == probe_query[TCP].ack:
        return "A"
    if probe_answer[TCP].seq == probe_query[TCP].ack + 1:
        return "A+"
    return "O"


def run_a_test(probe_query: IP, probe_answer: IP):
    """
    Run the "TCP acknowledgement number" test (A)
    This test is the same as S except that it tests how the acknowledgment number in the response compares to the
    sequence number in the respective probe.
    A value is returned according to the table at: https://nmap.org/book/osdetect-methods.html#osdetect-tbl-ack
    """
    assert probe_query.haslayer(TCP) and probe_answer.haslayer(TCP)
    if probe_answer[TCP].ack == 0:
        return "Z"
    if probe_answer[TCP].ack == probe_query[TCP].seq:
        return "S"
    if probe_answer[TCP].ack == probe_query[TCP].seq + 1:
        return "S+"
    return "O"


def run_f_test(probe_answer: IP):
    """
    Run the "TCP flags" test (F)
    Return a string representation of the tcp flags in the bits.
    For further information see: https://nmap.org/book/osdetect-methods.html#osdetect-flags
    """
    assert probe_answer.haslayer(TCP)
    result = ""
    tcp_flags_order = ['E', 'U', 'A', 'P', 'R', 'S', 'F']
    for tcp_flag in tcp_flags_order:
        if tcp_flag in probe_answer[TCP].flags:
            result += tcp_flag
    return result


def run_rd_test(probe_answer: IP):
    """
    Run the "RST data checksum" test (RD)
    Some operating systems return ASCII data such as error messages in reset packets.
    Return the crc32 of the packet's raw data if the given packet is an RST packet with data, otherwise 0
    For further information see: https://nmap.org/book/osdetect-methods.html#osdetect-rd
    """
    assert probe_answer.haslayer(TCP)

    if 'R' not in probe_answer[TCP].flags or not probe_answer.haslayer(Raw):
        return 0
    return crc32(probe_answer[Raw].load)
