import math
from dataclasses import dataclass, field
from functools import reduce
from typing import List, Mapping, Union

from scapy.layers.inet import IP, TCP
from scapy.plist import SndRcvList

from osscan.common.fingerprint import Test

from . import utils


@dataclass
class SeqTestInfo:
    """
    Holds extracts data from seq probes query and answers
    """
    seq_ipids: List[int] = field(default_factory=list)
    seq_diff: List[int] = field(default_factory=list)
    timestamps: List[int] = field(default_factory=list)
    ts_diff: List[int] = field(default_factory=list)
    time_usec_diffs: List[int] = field(default_factory=list)
    seq_rates: List[int] = field(default_factory=list)
    closed_ipids: List[int] = field(default_factory=list)
    icmp_ipids: List[int] = field(default_factory=list)
    seq_avg_rate: int = 0


def preprocess_probes_into_seq_info(probes: Mapping[str, SndRcvList]) -> SeqTestInfo:
    """
    Extract and process data that is relevant for the seq tests from the probes queries and answers
    """
    seq_info = SeqTestInfo()
    seq_probes = probes["SEQ"]

    # process seq probes
    for index, (query, answer) in enumerate(seq_probes):
        seq_info.seq_ipids.append(answer[IP].id)
        timestamp = utils.get_tsval_option(answer)
        seq_info.timestamps.append(timestamp)
        if index > 0:
            prev_query, prev_answer = seq_probes[index-1]
            # ipid diffs
            current_seq_diff = utils.mod_diff(answer[TCP].seq, prev_answer[TCP].seq)
            seq_info.seq_diff.append(current_seq_diff)
            # time diffs
            prev_timestamp = utils.get_tsval_option(prev_answer)
            # timestamps can be None, in that case treat them as zeros
            seq_info.ts_diff.append(utils.mod_diff(prev_timestamp or 0, timestamp or 0))
            senttime_diff = utils.usec_diff(query.sent_time, prev_query[IP].sent_time)
            seq_info.time_usec_diffs.append(senttime_diff)
            # seq rates
            seq_rate = current_seq_diff * 1000000.0 / senttime_diff
            seq_info.seq_rates.append(seq_rate)
            seq_info.seq_avg_rate += seq_rate
    seq_info.seq_avg_rate /= len(seq_info.seq_rates)

    # Three last TCP probes are sent to a closed port: T5, T6, and T7
    seq_info.closed_ipids = [tcp_closed_probe.answer[IP].id for tcp_closed_probe in probes["TCP"][:-3]
                             if tcp_closed_probe is not None]
    seq_info.icmp_ipids = [icmp_echo_probe.answer[IP].id for icmp_echo_probe in probes["IE"]]

    return seq_info


def run_gcd_test(test_info: SeqTestInfo) -> int:
    """
    Run the "TCP ISN greatest common divisor" test (GCD)
    This test attempts to determine the smallest number by which the target host increments the ISN values. For example,
    many hosts (especially old ones) always increment the ISN in multiples of 64,000.
    Return the greatest common divisor of all those elements. This GCD is also used for calculating the SP result.
    For further information see: https://nmap.org/book/osdetect-methods.html#osdetect-gcd
    """
    return reduce(lambda x, y: math.gcd(x, y), test_info.seq_diff)


def run_isr_test(test_info: SeqTestInfo) -> int:
    """
    Run the "TCP ISN counter rate" test (ISR)
    This value reports the average rate of increase for the returned TCP initial sequence number.
    For the average rate calculation see `seq_avg_rate` in `preprocess_probes_into_seq_info` above
    If that average is less than one (e.g. a constant ISN is used), ISR is zero. Otherwise ISR is eight times the binary
    logarithm (log base-2) of that average value, rounded to the nearest integer.
    For further information see: https://nmap.org/book/osdetect-methods.html#osdetect-isr
    """
    return math.ceil((math.log(test_info.seq_avg_rate) / math.log(2)) * 8)


def run_sp_test(seq_gcd: int, test_info: SeqTestInfo) -> int:
    """
    Run the "TCP ISN sequence predictability index" test (SP)
    This type of operating system probe attempts to determine an estimate for how predictable
    the sequence number generation algorithm is for a remote host.
    Statistical techniques, such as standard deviation, can be used to determine how predictable
    the sequence number generation is for a system.
    """
    sp = 0
    div_gcd = seq_gcd if seq_gcd > 9 else 1
    seq_stddev = 0
    for rate in test_info.seq_rates:
        tmp = rate / div_gcd - test_info.seq_avg_rate / div_gcd
        seq_stddev = tmp * tmp

    # We divide by num of seq_rates - 1, because that gives a better approx of stddev
    # when you're only looking at a subset of whole population
    seq_stddev /= len(test_info.seq_rates) - 1
    # than take the square root
    seq_stddev = math.sqrt(seq_stddev)
    # finally we take a binary logarithm, multiply by 8 and round
    if seq_stddev > 1:
        seq_stddev = math.log(seq_stddev) / math.log(2.0)
        sp = math.ceil(seq_stddev * 8)
    return sp


def run_ipid_test(ipids: List[int], min_ipids: int) -> Union[str, int]:
    """
    Run "IPID sequence generation algorithm" tests (TI, CI, II)
    This tests follows the following algorithm:
    1. If all of the ID numbers are zero, the value of the test is Z.
    2. If the IP ID sequence ever increases by at least 20,000, the value is RD (random). This result isn't possible for II because there are not enough samples to support it.
    3. If all of the IP IDs are identical, the test is set to that value in hex.
    4. If any of the differences between two consecutive IDs exceeds 1,000, and is not evenly divisible by 256,
       the test's value is RI (random positive increments). If the difference is evenly divisible by 256,
       it must be at least 256,000 to cause this RI result.
    5. If all of the differences are divisible by 256 and no greater than 5,120, the test is set to BI
       (broken increment). This happens on systems like Microsoft Windows where the IP ID is sent in host byte order
       rather than network byte order. It works fine and isn't any sort of RFC violation,
       though it does give away host architecture details which can be useful to attackers.
    6. If all of the differences are less than ten, the value is I (incremental). We allow difference up to ten here
       (rather than requiring sequential ordering) because traffic from other hosts can cause sequence gaps.
    7. If none of the previous steps identify the generation algorithm, the test is omitted from the fingerprint.

    For further information see: https://nmap.org/book/osdetect-methods.html#osdetect-ti
    """
    good_ipids = utils.filter_bad_ipids(ipids)
    if len(good_ipids) < min_ipids:
        return Test.OMIT

    # if all ipid are identical return the ipid incase it's non-zero or "Z" if it's zero
    first_ipid = good_ipids[0]
    if all(first_ipid == ipid for ipid in good_ipids):
        return "Z" if 0 == first_ipid else first_ipid

    is_broken_inc = True
    is_inc_by_2 = True
    is_inc = True
    for i in range(0, len(good_ipids)-1):
        diff = good_ipids[i + 1] - good_ipids[i]
        if diff >= 20000:
            return "RD"
        if diff > 1000 and (diff % 256 != 0 or (diff % 256 == 0 and diff >= 25600)):
            return "RI"
        if diff > 5120 or diff % 256 != 0:
            is_broken_inc = False
        if diff % 2 != 0:
            is_inc_by_2 = False
        if diff >= 10:
            is_inc = False

    if is_broken_inc:
        return "BI"
    if is_inc or is_inc_by_2:
        return "I"
    # Unknown - signal to omit the test
    return Test.OMIT


def run_ss_test(test_info: SeqTestInfo, tcp_ipid_class: str, icmp_ipid_class: str) -> str:
    """
    Run the "Shared IPID sequence boolean" test (SS)
    This Boolean value records whether the target shares its IP ID sequence between the TCP and ICMP protocols.
    For further information see: https://nmap.org/book/osdetect-methods.html#osdetect-ss
    """
    if utils.is_ipid_class_incremental(icmp_ipid_class) and utils.is_ipid_class_incremental(tcp_ipid_class):
        avg_seq_ipid = (test_info.seq_ipids[-1] - test_info.seq_ipids[0]) / len(test_info.seq_ipids)
        if test_info.icmp_ipids[0] < test_info.seq_ipids[-1] + 3 * avg_seq_ipid:
            return "S"
        else:
            return "O"
    return Test.OMIT


def run_ts_test(test_info: SeqTestInfo) -> Union[str, int]:
    """
    Run the "TCP timestamp option algorithm" test (TS)
    This tests follows the following algorithm:
    1. If any of the responses have no timestamp option, TS is set to U (unsupported).
    2. If any of the timestamp values are zero, TS is set to 0.
    3. If the average increments per second falls within the ranges 0-5.66, 70-150, or 150-350, TS is set to 1, 7, or 8,
       respectively. These three ranges get special treatment because they correspond to the 2 Hz, 100 Hz,
       and 200 Hz frequencies used by many hosts.
    4. In all other cases, Nmap records the binary logarithm of the average increments per second,
       rounded to the nearest integer. Since most hosts use 1,000 Hz frequencies, A is a common result.
    For further information see: https://nmap.org/book/osdetect-methods.html#osdetect-ts
    """
    if None in test_info.timestamps:
        return "U"

    if 0 in test_info.timestamps:
        return "Z"

    avg_ts_hz = 0.0
    for (ts_diff, time_usec_diff) in zip(test_info.ts_diff, test_info.time_usec_diffs):
        dhz = ts_diff / (time_usec_diff / 1000000.0)
        avg_ts_hz += dhz / len(test_info.ts_diff)

    if 0 <= avg_ts_hz <= 5.66:
        return 1
    elif 70 <= avg_ts_hz <= 150:
        return 7
    elif 150 <= avg_ts_hz <= 350:
        return 8
    else:
        return math.ceil(math.log(avg_ts_hz, 2))
