"""
Contains tests that are relevant for udp probes (So basically tests that are specifically for the U1 test line)
"""

from typing import Union

from scapy.layers.inet import ICMP, IP, UDP, IPerror, UDPerror
from scapy.packet import Raw


def run_ipl_test(probe_answer: IP) -> int:
    """
    Run the "IP total length" test (IPL)
    This test records the total length (in octets) of an IP packet.
    For further information see: https://nmap.org/book/osdetect-methods.html#osdetect-ipl
    """
    return probe_answer[IP].len


def run_un_test(probe_answer: IP) -> int:
    """
    Run the "Unused port unreachable field nonzero" test (UN)
    An ICMP port unreachable message header is eight bytes long, This test returns the last four bytes
    For further information see: https://nmap.org/book/osdetect-methods.html#osdetect-un
    """
    return probe_answer[ICMP].reserved & 0xffffffff


def run_ripl_test(probe_answer: IP) -> Union[int, str]:
    """
    Run the "Returned probe IP total length value" test (RIPL)
    ICMP port unreachable messages (as are sent in response to the U1 probe) are required to include the IP header which
    generated them. This test simply records the returned IP total length value. If the correct value of 0x148 (328) is
    returned, the value G (for good) is stored instead of the actual value.
    For further information see: https://nmap.org/book/osdetect-methods.html#osdetect-ripl
    """
    # ICMP port unreachable messages are required to include the IP header which generated them.
    answer_ip_err = probe_answer[ICMP][IPerror]
    return "G" if answer_ip_err.len == 328 else answer_ip_err.len


def run_rid_test(probe_query: IP, probe_answer: IP) -> Union[int, str]:
    """
    Run the "Returned probe IP ID value" test (RID)
    For further information see: https://nmap.org/book/osdetect-methods.html#osdetect-rid
    """
    # ICMP port unreachable messages are required to include the IP header which generated them.
    answer_ip_err = probe_answer[ICMP][IPerror]
    return "G" if probe_query[IP].id == answer_ip_err.id else answer_ip_err.id


def run_ripck_test(probe_answer: IP) -> str:
    """
    Run the "Integrity of returned probe IP checksum value" test (RIPCK)
    For further information see: https://nmap.org/book/osdetect-methods.html#osdetect-
    """
    def _recalculate_checksum(packet: IP):
        original_checksump = packet[IP].chksum
        packet = packet.__class__(bytes(packet))
        result = packet.chksum
        packet.chksum = original_checksump
        return result

    checksum = probe_answer[IP].chksum
    if checksum == 0:
        return "Z"

    return "G" if probe_answer.chksum == _recalculate_checksum(probe_answer) else "I"


def run_ruck_test(probe_query: IP, probe_answer: IP) -> str:
    """
    Run the "Integrity of returned probe UDP checksum" test (RUCK)
    The UDP header checksum value should be returned exactly as it was sent. If it is, G is recorded for this test.
    Otherwise the value actually returned is recorded.
    For further information see: https://nmap.org/book/osdetect-methods.html#osdetect-ruck
    """
    return "G" if probe_answer[UDPerror].chksum == UDP(bytes(probe_query[UDP])).chksum else probe_answer[UDPerror].chksum


def run_rud_test(probe_answer: IP) -> str:
    """
    Run the "Integrity of returned UDP data" test (RUD)
    This test checks the integrity of the (possibly truncated) returned UDP payload. If all the payload bytes are the
    expected ‘C’ (0x43), or if the payload was truncated to zero length, G is recorded; otherwise, I (invalid)
    is recorded.
    For further information see: https://nmap.org/book/osdetect-methods.html#osdetect-rud
    """
    if not probe_answer.haslayer(Raw):
        return "G"
    answer_payload = probe_answer[Raw].load
    return "G" if answer_payload.count(b'C') == len(answer_payload) else "I"
