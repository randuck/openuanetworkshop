"""
Contains tests that are used by different test lines
"""
from scapy.layers.inet import ICMP, IP, TCP

from osscan.common.fingerprint import Test

from . import utils


def run_o_test(probe_answer: IP) -> str:
    """
    Run the "TCP options" test (O)
    This test records a string representation of the given probe answer's TCP header options.
    It preserves the original ordering and also provides some information about option values. Because RFC 793 doesn't
    require any particular ordering, implementations often come up with unique orderings.
    Supported options and arguments are all shown in: https://nmap.org/book/osdetect-methods.html#osdetect-tbl-o
    """
    assert probe_answer.haslayer(TCP)

    result = ""
    for (tcp_option, value) in probe_answer[TCP].options:
        if tcp_option == "EOL":
            result += 'L'
        elif tcp_option == "NOP":
            result += 'N'
        elif tcp_option == "MSS":
            result += f'M{value:X}'
        elif tcp_option == "WScale":
            result += f'W{value:X}'
        elif tcp_option == "Timestamp":
            ts_val, ts_ecr = value
            result += f'T{1 if ts_val else 0}{1 if ts_ecr else 0}'
        elif tcp_option == "SAckOK":
            result += 'S'
    return result


def run_w_test(probe_answer: IP) -> str:
    """
    Run the"TCP Window size" test (W)
    This test simply records the 16-bit TCP window size of the received packet
    For further information see: https://nmap.org/book/osdetect-methods.html#osdetect-w
    """
    assert probe_answer.haslayer(TCP)
    return probe_answer[TCP].window


def run_df_test(probe_answer: IP) -> str:
    """
    Run the "IP don't fragment bit" test (DF)
    whether given probe response has the dont fragment turned on or not
    For further information see: https://nmap.org/book/osdetect-methods.html#osdetect-df
    """
    return utils.encode_bool('DF' in probe_answer[IP].flags)


def run_t_test(probe_answer: IP, u1_probe_query: IP, u1_probe_answer: IP) -> int:
    """
    Run the "IP Initial time-to-live" test (T)
    We try to determine how many hops away it is from the target by examining the ICMP port unreachable response to the
    U1 probe. That response includes the original IP packet, including the already-decremented TTL field, received by
    the target. By subtracting that value from our as-sent TTL, we learn how many hops away the machine is.
    We then add that hop distance to the probe response TTL to determine what the initial TTL was when that ICMP probe
    response packet was sent. The initial ttl is return on success, otherwise OMIT value
    For further information see: https://nmap.org/book/osdetect-methods.html#osdetect-t
    """
    # make sure probe answer is an ICMP unreachable as expected
    if not u1_probe_answer.haslayer(ICMP) or u1_probe_answer[ICMP].code != 3 or u1_probe_answer[ICMP].type != 3:
        return Test.OMIT

    # calculate actual distance using u1 icmp unreachable response
    distance = u1_probe_query[IP].ttl - u1_probe_answer[ICMP].ttl + 1
    return probe_answer[IP].ttl + distance - 1


def run_tg_test(probe_answer: IP) -> int:
    """
    Run the "IP initial time-to-live guess" test (TG)
    Most systems send packets with an initial TTL of 32, 60, 64, 128, or 255. So the TTL value received in the response
    is rounded up to the next value out of 32, 64, 128, or 255.
    Used as a fallback if the actual TTL [(T) test's value] was discovered.
    For further information see: https://nmap.org/book/osdetect-methods.html#osdetect-tg
    """
    ttl = probe_answer[IP].ttl
    if ttl <= 32:
        return 32
    elif ttl <= 64:
        return 64
    elif ttl <= 128:
        return 128
    return 255


def run_q_test(probe_answer: IP) -> str:
    """
    Run the "TCP miscellaneous quirks" test (Q)
    This tests for two quirks that a few implementations have in their TCP stack.
    The first is that the TCP reserved field is non-zero and the seconds is that the urgent field is not zero
    (assuming that the probe query did not contains the URG flag)
    for more information see: https://nmap.org/book/osdetect-methods.html#osdetect-q
    """
    assert probe_answer.haslayer(TCP)
    result = ""
    if probe_answer[TCP].reserved != 0:
        result += "R"
    if probe_answer[TCP].urgptr != 0:
        result += "U"
    return result
