import bisect
from collections import namedtuple
from typing import Iterable, List

from .common.fingerprint import FingerPrint

MAX_MATCHES = 5

MatchTuple = namedtuple("MatchTuple", ['confidence', 'fingerprint_description'])


class FingerPrintMatcher:
    def __init__(self, fingerprint_db: Iterable[FingerPrint], max_matches: int = MAX_MATCHES):
        assert hasattr(fingerprint_db, "tests_points")

        self.db = fingerprint_db
        self.max_matches = max_matches
        self.best_matches: List[MatchTuple] = []
        self.examined_count = 0

    def match(self, target_fingerprint: FingerPrint):
        self.best_matches.clear()
        self.examined_count = 0

        for fingerprint in self.db:
            confidence_factor = fingerprint.match(target_fingerprint, self.db.tests_points)
            match = MatchTuple(confidence_factor, fingerprint.description)

            # just insert the first MAX_MATCHES (because length of self.best_match is less than MAX_MATCHES)
            if self.examined_count < MAX_MATCHES:
                bisect.insort(self.best_matches, match)
            else:
                # incase the current match is better than the worst-best found match replace them
                min_best_confidence = self.best_matches[0].confidence
                if confidence_factor > min_best_confidence:
                    bisect.insort(self.best_matches, match)
                    del self.best_matches[0]

            self.examined_count += 1

    @property
    def best_match_description(self) -> str:
        return self.best_matches[-1].fingerprint_description

    @property
    def best_match_confidence(self) -> int:
        return round(self.best_matches[-1].confidence * 100, 3)

    def summary(self) -> str:
        """
        Returns a summary of the last match attempt which contains:
        * The number of db fingerprints it matched against.
        * A description of the operating systems that matched with the highest confidence
        """
        summary = f"{'-'*10} SUMMARY {'-'*10}\n"
        summary += f' Examined {self.examined_count} fingerprints and found the following matches:\n'
        for i, match in enumerate(self.best_matches[::-1]):
            summary += f' #{i+1} [{match.fingerprint_description}] with confidence: {match.confidence * 100:.3f}%\n'
        return summary
