import logging

from colorama import Fore, Style
from colorama import init as colorama_init

SUCCESS_LEVEL = logging.INFO + 1
GUESS_LEVEL = logging.INFO + 2


def addLoggingLevel(levelName, levelNum, methodName=None):
    """
    Comprehensively adds a new logging level to the `logging` module and the
    currently configured logging class.

    `levelName` becomes an attribute of the `logging` module with the value
    `levelNum`. `methodName` becomes a convenience method for both `logging`
    itself and the class returned by `logging.getLoggerClass()` (usually just
    `logging.Logger`). If `methodName` is not specified, `levelName.lower()` is
    used.

    To avoid accidental clobberings of existing attributes, this method will
    raise an `AttributeError` if the level name is already an attribute of the
    `logging` module or if the method name is already present

    Example
    -------
    >>> addLoggingLevel('TRACE', logging.DEBUG - 5)
    >>> logging.getLogger(__name__).setLevel("TRACE")
    >>> logging.getLogger(__name__).trace('that worked')
    >>> logging.trace('so did this')
    >>> logging.TRACE
    5

    """
    if not methodName:
        methodName = levelName.lower()

    if hasattr(logging, levelName):
        raise AttributeError('{} already defined in logging module'.format(levelName))
    if hasattr(logging, methodName):
        raise AttributeError('{} already defined in logging module'.format(methodName))
    if hasattr(logging.getLoggerClass(), methodName):
        raise AttributeError('{} already defined in logger class'.format(methodName))

    # This method was inspired by the answers to Stack Overflow post
    # http://stackoverflow.com/q/2183233/2988730, especially
    # http://stackoverflow.com/a/13638084/2988730
    def logForLevel(self, message, *args, **kwargs):
        if self.isEnabledFor(levelNum):
            self._log(levelNum, message, args, **kwargs)

    def logToRoot(message, *args, **kwargs):
        logging.log(levelNum, message, *args, **kwargs)

    logging.addLevelName(levelNum, levelName)
    setattr(logging, levelName, levelNum)
    setattr(logging.getLoggerClass(), methodName, logForLevel)
    setattr(logging, methodName, logToRoot)


class ColoredFormatter(logging.Formatter):
    """
    custom log message formatter that colors every log message according to its log level
    """
    debug_fmt = "(%(filename)s:%(lineno)d) "
    format = "%(message)s"

    FORMATS = {
        logging.DEBUG: f'{Fore.LIGHTBLACK_EX} [*] {format} {debug_fmt}',
        logging.INFO: f'{Fore.WHITE} [+] {format}',
        logging.WARNING: f'{Fore.YELLOW} [!] {format}',
        logging.ERROR: f'{Fore.RED} [X] {format}',
        logging.CRITICAL: f'{Style.BRIGHT}{Fore.RED} [X] {format}{Style.NORMAL}',
        # custom
        SUCCESS_LEVEL: f'{Style.BRIGHT}{Fore.GREEN} [V] {format}{Style.NORMAL}',
        GUESS_LEVEL: f'{Style.BRIGHT}{Fore.CYAN} [V] {format}{Style.NORMAL}',
    }

    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)


def logging_init(verbose: bool):
    """
    Configures project's logger
    """
    colorama_init(autoreset=True)

    # add colored format handler
    ch = logging.StreamHandler()
    ch.setFormatter(ColoredFormatter())
    # configure logging
    logging.basicConfig(level=logging.DEBUG if verbose else logging.INFO, handlers=[ch])
    # custom log levels
    addLoggingLevel("SUCCESS", SUCCESS_LEVEL)
    addLoggingLevel("GUESS", GUESS_LEVEL)
