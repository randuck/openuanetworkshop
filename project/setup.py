from setuptools import find_packages, setup

setup(
    name="osscan",
    version="0.1",
    packages=find_packages(),
    package_data={
        "": ["nmap_os_db_parser/nmap-os-db"]
    },
    install_requires=[
        'click >= 8.1.3',
        'scapy >= 2.4.5',
        'colorama >= 0.4.6',
    ],
    entry_points={
        'console_scripts': [
            'osscan=osscan.main:main',
        ]
    }
)
