# OpenuaNetWorkshop

Project and related material for `networks workshop` in openua (20588)

https://www.openu.ac.il/courses/20588.htm

## Description
The workshop is divided into two parts:
- [x] 40% Presentation about a topic that is picked from a list of predefined options (including code snippets).
- [x] 60% Project that relates to networking.

### Presentation
The chosen topic is composed from two subjects: `ICMP and Raw Sockets & Libpcap` while my part is the latter.  
The relevant material can be found under `presentation` folder as expected.

### Project
OS Fingerprinring - A python package named `osscan` which attempts to mimic nmap's active os fingerprinting capabilities (`nmap -O`)

